<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?=url("")?>assets-front/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=url("")?>assets-front/plugins/slick/slick.css">
<link rel="stylesheet" href="<?=url("")?>assets-front/plugins/slick/slick-theme.css">
<!-- icon css-->
<link rel="stylesheet" href="<?=url("")?>assets-front/plugins/elagent-icon/style.css">
<link rel="stylesheet" href="<?=url("")?>assets-front/plugins/animation/animate.css">
<link rel="stylesheet" href="<?=url("")?>assets-front/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="<?=url("")?>assets-front/css/style.css">
<link rel="stylesheet" href="<?=url("")?>assets-front/css/responsive.css">