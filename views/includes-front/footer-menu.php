<div class="col-lg-3 col-sm-6">
	<div class="f_widget link_widget pl_30">
		<h3 class="f_title">Company</h3>
		<ul class="list-unstyled link_list">
			<li><a href="#">About Us</a></li>
			<li><a href="#">Testimonials</a></li>
			<li><a href="#">Affiliates</a></li>
			<li><a href="#">Partners</a></li>
			<li><a href="#">Careers</a></li>
			<li><a href="#">Signature Anywhere for Good</a></li>
			<li><a href="#">Contact Us</a></li>
		</ul>
	</div>
</div>

<div class="col-lg-2 col-sm-6">
	<div class="f_widget link_widget">
		<h3 class="f_title">Support</h3>
		<ul class="list-unstyled link_list">
			<li><a href="index-3.html">Help Desk</a></li>
			<li><a href="#">Knowledge Base</a></li>
			<li><a href="#">Live Chat</a></li>
			<li><a href="#">Integrations</a></li>
			<li><a href="#">Reports</a></li>
			<li><a href="#">iOS & Android</a></li>
			<li><a href="#">Messages</a></li>
		</ul>
	</div>
</div>

<div class="col-lg-3 col-sm-6">
	<div class="f_widget link_widget pl_70">
		<h3 class="f_title">Doc Pages</h3>
		<ul class="list-unstyled link_list">
			<li><a href="doclist.html">Doc Topic</a></li>
			<li><a href="#">Free Training</a></li>
			<li><a href="doc-archive.html">Doc Archive</a></li>
			<li><a href="changelog.html">Changelog</a></li>
			<li><a href="Onepage.html">Onepage Docs</a></li>
			<li><a href="#">Conversion Tracking</a></li>
			<li><a href="cheatsheet.html">Cheatseet</a></li>
		</ul>
	</div>
</div>