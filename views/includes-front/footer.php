<footer class="footer_area f_bg_color">
	<img class="p_absolute leaf" src="<?=url("")?>assets-front/img/v.svg" alt="">
	<img class="p_absolute f_man" src="<?=url("")?>assets-front/img/home_two/f_man.png" alt="">
	<img class="p_absolute f_cloud" src="<?=url("")?>assets-front/img/home_two/cloud.png" alt="">
	<img class="p_absolute f_email" src="<?=url("")?>assets-front/img/home_two/email-icon.png" alt="">
	<img class="p_absolute f_email_two" src="<?=url("")?>assets-front/img/home_two/email-icon_two.png" alt="">
	<img class="p_absolute f_man_two" src="<?=url("")?>assets-front/img/home_two/man.png" alt="">

	<div class="footer_top">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-sm-6">
					<div class="f_widget subscribe_widget">
						<a href="<?=url("")?>" class="f_logo">
							<img src="<?=url("")?>assets-front/img/signature_anywhere-footer.png" style="max-width: 300px" alt="">
						</a>

						<h4 class="c_head">Subscribe to our newsletter</h4>
						<form action="#" class="footer_subscribe_form">
							<input type="email" placeholder="Email" class="form-control">
							<button type="submit" class="s_btn">Send</button>
						</form>

						<ul class="list-unstyled f_social_icon">
							<li><a href="#"><i class="social_facebook"></i></a></li>
							<li><a href="#"><i class="social_twitter"></i></a></li>
							<li><a href="#"><i class="social_vimeo"></i></a></li>
							<li><a href="#"><i class="social_linkedin"></i></a></li>
						</ul>
					</div>
				</div>

				<!-- Footer Menu start -->
				{{ view("includes-front/footer-menu"); }}
			</div>

			<div class="border_bottom"></div>
		</div>
	</div>

	<div class="footer_bottom text-center">
		<div class="container">
			<p>© {{date('Y')}} All Rights Reserved by <a href="<?=url("")?>">Signature Anywhere</a></p>
		</div>
	</div>
</footer>