<nav class="navbar navbar-expand-lg menu_one" id="stickyTwo">
	<div class="container">
		<a class="navbar-brand sticky_logo" href="<?=url("")?>">
			<img src="<?=url("")?>assets-front/img/logo-w.png" srcset="<?=url("")?>assets-front/img/signature_anywhere-head.png" style="max-width:300px" alt="logo">
			<img src="<?=url("")?>assets-front/img/logo.png" srcset="<?=url("")?>assets-front/img/signature_anywhere-head.png" style="max-width:300px" alt="logo">
		</a>

		<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="menu_toggle">
				<span class="hamburger">
					<span></span>
					<span></span>
					<span></span>
				</span>

				<span class="hamburger-cross">
					<span></span>
					<span></span>
				</span>
			</span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav menu ml-auto">
				<li class="nav-item dropdown submenu active">
					<a href="<?=url("")?>" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Home
					</a>
				</li>
				<li class="nav-item dropdown submenu mega_menu tab-demo">
					<a href="<?=url("Home@about")?>" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						About Us
					</a>
				</li>
				<li class="nav-item dropdown submenu">
					<a class="nav-link dropdown-toggle" href="<?=url("Home@services")?>" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Services
					</a>
				</li>
				<li class="nav-item dropdown submenu">
					<a class="nav-link dropdown-toggle" href="<?=url("Home@price_planes")?>" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Price Planes
					</a>
				</li>
				<li class="nav-item dropdown submenu">
					<a class="nav-link dropdown-toggle" href="<?=url("Home@contact")?>" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Contact Us
					</a>
				</li>
			</ul>

            @if(!empty($user))
			<a class="nav_btn" href="<?=url("Home@logout")?>">Sign Out</a>
            @else
			<a class="nav_btn" href="<?=url("Home@sign_up")?>">Sign Up</a>
            @endif
		</div>
	</div>
</nav>