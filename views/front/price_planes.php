<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="<?=url("")?>assets-front/img/favicon.ico" type="image/x-icon">

	<!-- header styles start -->
	{{ view("includes-front/header-styles"); }}

	<title>Signature Anywhere | Price Planes</title>
</head>

<body class="sticky-nav-doc doc" data-scroll-animation="true">
    <!-- Page Loader -->
    {{ view("includes-front/page-loader") }}

    <!--** Body Wrapper Start **-->
    <div class="body_wrapper">
        <!-- Nav Menu start -->
        {{ view("includes-front/header-menu"); }}

        <!--** Page Banner & Breadcrumbs Start **-->
        <section class="breadcrumb_area breadcrumb_area_four">
            <img class="p_absolute bl_left" src="<?=url("")?>assets-front/img/v.svg" alt="">
            <img class="p_absolute bl_right" src="<?=url("")?>assets-front/img/home_one/b_leaf.svg" alt="">
            <img class="p_absolute one wow fadeInRight" src="<?=url("")?>assets-front/img/home_one/b_man_two.png" alt="">

            <div class="container">
                <div class="breadcrumb_content_two text-center">
                    <h2>Price Planes</h2>

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?=url("")?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Price Planes</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </section>
        <!--** Page Banner & Breadcrumbs End **-->

        <!--** Page Content Start **-->
        <section class="contact_area sec_pad">
            <div class="container">

                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>

                <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>

                <p>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</p>

                <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>

                <p>Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo.</p>

                <p>Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi</p>

            </div>
        </section>
        <!--** Page Content Start **-->

        <!-- Footer start -->
        {{ view("includes-front/footer"); }}
    </div>
    <!--** Body Wrapper End **-->

    <!-- Back to top button -->
    <a id="back-to-top" title="Back to Top"></a>

    <!-- Footer scripts start -->
    {{ view("includes-front/footer-scripts"); }}
</body>
</html>