<?php
/**
 * Created by PhpStorm.
 * User: Fast Computer
 * Date: 10/29/2020
 * Time: 5:35 PM
 */
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--    <meta name="_token" content="{{ csrf_token() }}" />-->
    <link rel="shortcut icon" href="<?=url("")?>assets-front/img/favicon.ico" type="image/x-icon">

    <!-- header styles start -->
    {{ view("includes-front/header-styles"); }}
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=B612+Mono:400,400i,700|Charm:400,700|EB+Garamond:400,400i,700|Noto+Sans+TC:400,700|Open+Sans:400,400i,700|Pacifico|Reem+Kufi|Scheherazade:400,700|Tajawal:400,700&amp;subset=arabic" crossorigin="anonymous" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Cookie|Courgette|Dr+Sugiyama|Grand+Hotel|Great+Vibes|League+Script|Meie+Script|Miss+Fajardose|Niconne|Pacifico|Petit+Formal+Script|Rochester|Sacramento|Tangerine" crossorigin="anonymous" rel="stylesheet">

    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/select2/css/select2.css" rel="stylesheet">
    <!-- Signature pad css -->
    <link rel="stylesheet" href="<?=url("");?>assets/css/signature-pad.css">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets-front/css/pdf_style.css" rel="stylesheet">
    <script src="<?=url("");?>assets/js/jscolor.js"></script>

    <title>Signature Anywhere | Sign Document</title>

    <style>
        .home_next_step, .home_start_step, .home_finish_step {
            margin-bottom:12px;
            position:absolute;
            top:20px;
            left:150px;
            display:none
        }
        .options_total {
            position: absolute;
            top: -25px;
            left: 236px;
            font-size: 18px;
        }

        /* Error Messages */
        .fieldEmptyErr {
            color: #ff1a1a;
            font-size: 15px;
            margin: 0;
            display: none;
        }
        .txtFieldErr {
            border:1px solid #ff1a1a;
        }

        /* Stripe Payment Modal */
        #divDocStripe {
            display: none;
        }
        .docStripePayment {
            border: 1px solid #DEE2E6;
            padding: 6px 8px;
            margin-bottom: 18px;
            border-radius: 4px;
        }
    </style>

    <link href="<?=url("");?>assets-front/css/views/modal-style.css" rel="stylesheet">
</head>

<body class="sticky-nav-doc doc open_template" data-scroll-animation="true">
    <!-- Page Loader -->
    {{ view("includes-front/page-loader") }}

    <!--** Body Wrapper Start **-->
    <div class="body_wrapper">
        <!-- Nav Menu start -->
        {{ view("includes-front/header-menu"); }}

        <!--** Page Banner & Breadcrumbs Start **-->
        <section class="breadcrumb_area breadcrumb_area_four">
            <img class="p_absolute bl_left" src="<?=url("")?>assets-front/img/v.svg" alt="">
            <img class="p_absolute bl_right" src="<?=url("")?>assets-front/img/home_one/b_leaf.svg" alt="">
            <img class="p_absolute one wow fadeInRight" src="<?=url("")?>assets-front/img/home_one/b_man_two.png" alt="">

            <div class="container">
                <div class="breadcrumb_content_two text-center">
                    <h2>Document</h2>

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?=url("")?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Document ({{ $document->name }})</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </section>
        <!--** Page Banner & Breadcrumbs End **-->

        <!--** Manage Fields & Edit Section Start **-->
        @if($document->public_permissions == "sign_edit")
        <section class="contact_area" style="background-color: #F3F3F3;">
            <div class="container">
                <div class="row">
                    @if ( is_object($request) && $request->status == "Pending" )
                    <li class="m-o">
                        <button class="btn btn-success accept-request">
                            <i class="ion-ios-checkmark-outline"></i>
                            <span>Accept</span>
                        </button>
                    </li>
                    <li class="m-o">
                        <button class="btn btn-danger send-to-server-click"
                                data="requestid:{{ $request->id }}|file:{{ $document->id }}|csrf-token:{{ csrf_token() }}"
                                url="<?=url("Guest@decline");?>"
                                warning-title="Are you sure?"
                                warning-message="This request will be declined and email sent to sender."
                                warning-button="Yes, Decline"
                                loader="true">
                            <i class="ion-ios-close-outline"></i> Decline
                        </button>
                    </li>
                    @else
                    @if($document->is_template == "Yes")
                    <div class="col-lg-6 col-md-6 col-sm-12 p-4 text-right page-actions">
                        <button type="button" class="btn action_btn thm_btn launch-editor" style="padding:9px 24px;font-size:14px">
                            <i class="ion-edit"></i> {{$lauchLabel}}
                        </button>

                        <button type="button" class="btn action_btn thm_btn requestToRecipient" style="padding:9px 24px;font-size:14px">
                            <i class="ion-ios-plus-outline"></i>
                            Request Sign
                        </button>
                    </div>
                    @endif
                    @endif
                </div>
            </div>
        </section>
        @endif
        <!--** Manage Fields & Edit Section End **-->

        <!--** View PDF Section Start **-->
        <section class="contact_area sec_pad">
            <div class="container">

                <div class="page-title">
                    <h3>{{ $document->name }}</h3>
                </div>

                <div class="light-card document">
                    <div class="signer-document">
                        @if ( $document->extension == "pdf" )
                        <!-- open PDF docements -->
                        <div class="document-pagination">
                            <div class="pull-left">
                                <button id="prev" class="btn btn-default btn-round"><i class="ion-ios-arrow-left"></i></button>
                                <button id="next" class="btn btn-default btn-round"><i class="ion-ios-arrow-right"></i></button>
                                <span class="text-muted ml-15">Page <span id="page_num">0</span> of <span id="page_count">0</span></span>
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-default btn-round btn-zoom" zoom="plus"><i class="ion-plus"></i></button>
                                <button class="btn btn-default btn-round btn-zoom" zoom="minus"><i class="ion-minus"></i></button>
                            </div>
                        </div>
                        <div class="document-load">
                            <div class="loader-box"><div class="circle-loader"></div></div>
                        </div>
                        <div class="document-error">
                            <i class="ion-android-warning text-danger"></i>
                            <p class="text-muted"><strong>Oops! </strong> <span class="error-message"> Something went wrong.</span></p>
                        </div>
                        <div class="text-center">
                            <div class="document-map"></div>
                            <canvas id="document-viewer"></canvas>
                        </div>
                        @else
                        <iframe src='https://view.officeapps.live.com/op/embed.aspx?src={{ env("APP_URL") }}/uploads/files/{{ $document->filename }}' width='100%' height='1000px' frameborder='0'></iframe>
                        @endif
                    </div>
                </div>
            </div>
        </section>

        <!--** PDF Tools Section Start **-->
        <div class="signer-overlay">
            <div class="signer-overlay-header">
                <div class="signer-overlay-action">
                    <button class="btn btn-responsive btn-default close-editor-overlay"><i class="ion-ios-close-outline"></i> Close </button>
                    <button class="btn btn-responsive btn-primary signer-save"><i class="ion-ios-checkmark-outline"></i> <span>Save</span> </button>
                    <button class="btn btn-responsive btn-primary signer-next" index="1"><i class="ion-ios-checkmark-outline"></i> <span>Next</span> </button>
                </div>

                <div class="signer-header-tools">
                    <div class="signer-header-tool-holder">
                        <div class="signer-tool" tool="signature" action="true">
                            <div class="tool-icon tool-signature"></div>
                            <p>Signature</p>
                        </div>

                        <div class="signer-tool" tool="stamp" action="true">
                            <div class="tool-icon tool-stamp"></div>
                            <p>R/Stamp</p>
                        </div>

                        <div class="signer-tool" tool="text" action="true">
                            <div class="tool-icon tool-text"></div>
                            <p>Text</p>
                        </div>

                        <div class="signer-tool" tool="draw" action="true">
                            <div class="tool-icon tool-draw"></div>
                            <p>Draw</p>
                        </div>

                        <div class="signer-tool" tool="image" action="true">
                            <div class="tool-icon tool-image"></div>
                            <p>Image</p>
                        </div>

                        <div class="signer-tool" tool="symbol" action="true">
                            <div class="tool-icon tool-symbols"></div>
                            <p>Symbols</p>
                        </div>

                        <div class="signer-tool" tool="shape" action="true">
                            <div class="tool-icon tool-shapes"></div>
                            <p>Shapes</p>
                        </div>

                        <div class="signer-tool" tool="fields" action="true">
                            <div class="tool-icon tool-fields"></div>
                            <p>Fields</p>
                        </div>

                        @if ( empty($request) )
                        <div class="signer-tool" tool="input" action="true">
                            <div class="tool-icon tool-textinput"></div>
                            <p>Input</p>
                        </div>
                        @endif

                        <div class="signer-tool" tool="rotate" action="true">
                            <div class="tool-icon tool-rotate"></div>
                            <p>Rotate</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="signer-more-tools">
                <div class="signer-tool" tool="alignleft" action="false" group="text">
                    <div class="tool-icon tool-alignleft"></div>
                    <p>Align Left</p>
                </div>

                <div class="signer-tool" tool="aligncenter" action="false" group="text">
                    <div class="tool-icon tool-aligncenter"></div>
                    <p>Align Center</p>
                </div>

                <div class="signer-tool" tool="alignright" action="false" group="text">
                    <div class="tool-icon tool-alignright"></div>
                    <p>Align Right</p>
                </div>

                <div class="signer-tool" tool="bold" action="false" group="text">
                    <div class="tool-icon tool-bold"></div>
                    <p>Bold</p>
                </div>

                <div class="signer-tool" tool="italic" action="false" group="text">
                    <div class="tool-icon tool-italic"></div>
                    <p>Italic</p>
                </div>

                <div class="signer-tool" tool="underline" action="false" group="text">
                    <div class="tool-icon tool-underline"></div>
                    <p>Underline</p>
                </div>

                <div class="signer-tool" tool="strikethrough" action="false" group="text">
                    <div class="tool-icon tool-strikethrough"></div>
                    <p>Strikethrough</p>
                </div>

                <div class="signer-tool" tool="font" action="false" group="text">
                    <div class="tool-icon tool-font"></div>
                    <p>Font</p>
                </div>

                <div class="signer-tool" tool="fontsize" action="false">
                    <div class="tool-icon tool-fontsize">
                        <div class="numberInput">
                            <input type="text" class="font-size" value="14" min="0" />
                            <span class="arrow up"></span>
                            <span class="arrow down"></span>
                        </div>
                    </div>
                    <p class="font-size-label">Font Size</p>
                </div>

                <div class="signer-tool" tool="color" action="false" color="#000000">
                    <div class="tool-icon tool-colorfill"><button id="color-picker" class="jscolor {valueElement:null,value:'000000', borderRadius:'1px', borderColor:'#e6eaee', onFineChange:'updateColor(this)'}"></button></div>
                    <p>Color</p>
                </div>

                <div class="signer-tool" tool="duplicate" action="false">
                    <div class="tool-icon tool-duplicate"></div>
                    <p>Duplicate</p>
                </div>

                <div class="signer-tool" tool="delete" action="false">
                    <div class="tool-icon tool-delete"></div>
                    <p>Delete</p>
                </div>
            </div>

            <div style="position: relative">
                @if(!empty($document->template_fields))
                <div class="options_total">
                    <strong>Total: </strong>
                    <span>{{$fields_count}}</span>
                </div>
                <button type="button" class="btn btn-success home_start_step">
                    Start
                </button>
                <button type="button" class="btn btn-primary home_next_step" data-doc_key="{{ $document->document_key }}" data-fields="0">
                    Next
                </button>
                <button type="button" class="btn btn-secondary home_finish_step" onclick="javascript:void(0)">
                    Finish
                </button>
                @endif

                <div class="signer-overlay-previewer col-md-8 light-card"> </div>
            </div>
            <div class="signer-overlay-footer">
                <p class="text-muted text-center"> Powered by <?=env("APP_NAME")?> | <?=date("Y")?> &copy; <?=env("APP_NAME")?> | All Rights Reserved. </p>
            </div>

            <div class="signer-assembler"></div>
            <div class="signer-builder"></div>
            <div class="request-helper">Select input fields and signature position for <strong></strong></div>
        </div>
        <!--** PDF Tools Section End **-->

        <!-- Footer start -->
        {{ view("includes-front/footer"); }}
    </div>
    <!--** Body Wrapper End **-->

    <!-- Back to top button -->
    <a id="back-to-top" title="Back to Top"></a>

    <!-- Footer scripts start -->
    {{ view("includes-front/footer-scripts"); }}

    <!-- For PDF -->
    <script src="<?=url("");?>assets/libs/dropify/js/dropify.min.js"></script>
    <script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=url("");?>assets/js/simcify.min.js"></script>
    <script src="<?=url("");?>assets/libs/html2canvas/html2canvas.js"></script>
    <script src="<?=url("");?>assets/libs/clipboard/clipboard.min.js"></script>
    <script src="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=url("");?>assets/libs/select2/js/select2.min.js"></script>
    <script src="<?=url("");?>assets/libs/tagsinput/bootstrap-tagsinput.js"></script>
    <script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
    <script src="<?=url("");?>assets/libs/jcanvas/jcanvas.min.js"></script>
    <script src="<?=url("");?>assets/js/dom-to-image.min.js"></script>
    <script src="<?=url("");?>assets/libs/jcanvas/signature.min.js"></script>
    <script src="<?=url("");?>assets/libs/jcanvas/editor.min.js"></script>
    <script src="<?=url("");?>assets/js/touch-punch.min.js"></script>
    <script src="<?=url("");?>assets/js/pdf.js"></script>

    <!-- PDF Custom Script-->
    <script type="text/javascript">
        var url = '<?=url("");?>uploads/files/{{ $document->filename }}',
            isTemplate = '{{ $document->is_template }}',
            postChatUrl = '<?=url("Chat@post");?>',
            settingsPage = '<?=url("Settings@get");?>',
            saveFieldsUrl = '<?=url("Field@save");?>',
            deleteFieldsUrl = '<?=url("Field@delete");?>',
            getChatUrl = '<?=url("Chat@fetch");?>',
            signHomeUrl = '<?=url("Home@nextRecipient_docSign");?>',
            sendRequestUrl = '<?=url("Request@send");?>',
            createTemplateUrl = '<?=url("Template@create");?>',
            baseUrl = '<?=url("");?>',
            auth = true;
        document_key = '{{ $document->document_key }}';
        PDFJS.workerSrc = '<?=url("");?>assets/js/pdf.worker.min.js';

        @if ( empty( $user->signature ) )
        var signature = '';
        @else
        var signature = '<?=url("");?>uploads/signatures/{{ $user->signature }}';
        @endif

        @if ( is_object($request) && $request->status == "Pending" )
        var signingKey = '{{ $request->signing_key }}';
        var requestPositions = '';
        var requestWidth = '';
        @else
        var signingKey = '';
        @endif

        @if ( $document->is_template == "Yes" )
        var savedWidth = {{ $savedWidth }};
        var templateFields = {{ $templateFields }};
        @endif

    </script>

    <script src="<?= url("") ?>assets-front/js/views/sign_document.js"></script>

    <!-- custom scripts -->
    <script src="<?=url("");?>assets/js/home-app.js"></script>
    <script src="<?=url("");?>assets/js/home-signer.js"></script>
    <script src="<?=url("");?>assets/js/home-render.js"></script>
</body>
</html>

