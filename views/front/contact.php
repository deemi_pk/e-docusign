<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="<?=url("")?>assets-front/img/favicon.ico" type="image/x-icon">

	<!-- header styles start -->
	{{ view("includes-front/header-styles"); }}

	<title>Signature Anywhere | Contact Us</title>
</head>

<body class="sticky-nav-doc doc" data-scroll-animation="true">
    <!-- Page Loader -->
    {{ view("includes-front/page-loader") }}

    <!--** Body Wrapper Start **-->
	<div class="body_wrapper">
		<!-- Nav Menu start -->
		{{ view("includes-front/header-menu"); }}

        <!--** Page Banner & Breadcrumbs Start **-->
		<section class="breadcrumb_area breadcrumb_area_four">
			<img class="p_absolute bl_left" src="<?=url("")?>assets-front/img/v.svg" alt="">
			<img class="p_absolute bl_right" src="<?=url("")?>assets-front/img/home_one/b_leaf.svg" alt="">
			<img class="p_absolute one wow fadeInRight" src="<?=url("")?>assets-front/img/home_one/b_man_two.png" alt="">

			<div class="container">
				<div class="breadcrumb_content_two text-center">
					<h2>Contact Us</h2>

					<nav aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?=url("")?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Contact</li>
						</ol>
					</nav>
				</div>
			</div>
		</section>
        <!--** Page Banner & Breadcrumbs End **-->

        <!--** Page Content Start **-->
		<section class="contact_area" style="padding-bottom: 100px">
			<div class="container">

				<div class="contact_info">
					<div class="section_title text-left">
						<h2 class="h_title wow fadeInUp">Let’s start the conversation</h2>
						<p>Please email us, we’ll happy to assist you.</p>
					</div>

					<form action="#" class="contact_form">
						<div class="row contact_fill">
							<div class="col-lg-4 form-group">
								<h6>Full Name</h6>
								<input type="text" class="form-control" name="name" id="name" placeholder="Enter your name here">
							</div>
							<div class="col-lg-4 form-group">
								<h6>Email</h6>
								<input type="email" class="form-control" name="email" id="email" placeholder="info@Signature Anywhere.com">
							</div>
							<div class="col-lg-4 form-group">
								<h6>Phone no</h6>
								<input type="tel" class="form-control" name="tel" id="phone" placeholder="+462">
							</div>
							<div class="col-lg-12 form-group">
								<h6>Message</h6>
								<textarea class="form-control message" id="message" placeholder="Enter Your Text ..."></textarea>
							</div>
							<div class="col-lg-12 form-group">
								<button type="submit" class="btn action_btn thm_btn">Send Message</button>
							</div>
						</div>
					</form>
				</div>

			</div>
		</section>
        <!--** Page Content End **-->

		<!-- Footer start -->
		{{ view("includes-front/footer"); }}
	</div>
    <!--** Body Wrapper End **-->

	<!-- Back to top button -->
	<a id="back-to-top" title="Back to Top"></a>

	<!-- Footer scripts start -->
	{{ view("includes-front/footer-scripts"); }}
</body>
</html>