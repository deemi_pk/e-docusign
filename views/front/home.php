<?php
/**
* Created by PhpStorm.
* User: Nadeem Arshad
* Date: 10/18/2020
* Time: 2:10 PM
*/
?>
<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="<?=url("")?>assets-front/img/favicon.ico" type="image/x-icon">

	<!-- header styles start -->
	{{ view("includes-front/header-styles") }}
	<link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">

	<title>Signature Anywhere</title>

	<style>
		.fieldEmptyErr {
			color: #ff1a1a;
			font-size: 15px;
			display: none;
		}
		.txtFieldErr {
			border:1px solid #ff1a1a;
		}
	</style>

    <link href="<?=url("");?>assets-front/css/views/modal-style.css" rel="stylesheet">
</head>

<body class="sticky-nav-doc doc" data-scroll-animation="true">
    <!-- Page Loader -->
    {{ view("includes-front/page-loader") }}

    <!--** Body Wrapper Start **-->
	<div class="body_wrapper">
		<!-- Nav Menu start -->
		{{ view("includes-front/header-menu",$data) }}

        <!--** Page Banner & Breadcrumbs Start **-->
		<section class="breadcrumb_area breadcrumb_area_four">
			<img class="p_absolute bl_left" src="<?=url("")?>assets-front/img/v.svg" alt="">
			<img class="p_absolute bl_right" src="<?=url("")?>assets-front/img/home_one/b_leaf.svg" alt="">
			<img class="p_absolute one wow fadeInRight" src="<?=url("")?>assets-front/img/home_one/b_man_two.png" alt="">

			<div class="container">
				<div class="breadcrumb_content_two text-center">
					<h2>Home Page</h2>
				</div>
			</div>
		</section>
        <!--** Page Banner & Breadcrumbs End **-->

        <!--** Upload Documents Start **-->
		<section class="drag_drop_file sec_pad">
			<div class="container">
<!--           upload_document,  upload_file_form    -->
				<form method="post" action="upload_document" enctype="multipart/form-data" id="upload_file_form">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-6 text-left" style="margin-left:176px">
							<strong>Only PDF, Word, Excel and Power Point allowed.</strong>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-6 form-group docNameErr" style="max-width:70.7%;margin-left:176px">
							<div class="small_text">File Name *</div>
							<input type="text" class="form-control" name="file_title" id="file_name" placeholder="File Name">
							<p class="fieldEmptyErr"></p>

							<input type="hidden" name="folder" value="1">
							<input type="hidden" name="csrf-token" value="{{ csrf_token() }}" />
						</div>
					</div>

					<!-- Drag and Drop container-->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-6 form-group docFileErr" style="max-width:70.7%;margin-left:176px">
							<label>Choose file *</label>
							<input type="file" name="file" class="dropify" id="doc_file" data-allowed-file-extensions="pdf <?php if(env("ALLOW_NON_PDF") == "Enabled"){ ?>doc docx ppt pptx xls xlsx <?php } ?>">
							<p class="fieldEmptyErr"></p>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-6 text-left" style="margin-left:176px">
							<strong>Please signup before upload file</strong>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-6 mt-5 text-right">
							<button type="submit" class="btn action_btn thm_btn" id="uploadDoc">Upload</button>
						</div>
					</div>
				</form>
			</div>
		</section>
        <!--** Upload Documents End **-->

		<section class="h_doc_documentation_area bg_color sec_pad">
			<div class="container">
				<div class="section_title text-center">
					<h2 class="h_title wow fadeInUp">The Best Online Documentation</h2>
					<p class="wow fadeInUp" data-wow-delay="0.4s">Loaded with awesome features like Documentation, Knowledgebase, Forum & more!</p>
				</div>

				<ul class="nav nav-tabs documentation_tab" id="myTabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="or" aria-selected="true">Signature Anywhere</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="local-tab" data-toggle="tab" href="#local" role="tab" aria-controls="doc" aria-selected="false">Sdoc</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="word-tab" data-toggle="tab" href="#word" role="tab" aria-controls="forum" aria-selected="false">Spider Docs</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="wiki-tab" data-toggle="tab" href="#wiki" role="tab" aria-controls="wiki" aria-selected="false">WikiDoc</a>
					</li>
				</ul>

				<div class="tab-content" id="myTabContents">
					<div class="tab-pane documentation_tab_pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
						<div class="row">
							<div class="col-lg-4">
								<div class="documentation_text">
									<div class="round wow fadeInUp">
										<img src="<?=url("")?>assets-front/img/home_one/icon/file1.png" alt="">
									</div>

									<h4 class="wow fadeInUp" data-wow-delay="0.2s">Build Any Page In Seconds</h4>
									<p class="wow fadeInUp" data-wow-delay="0.3s">Omnis voluptate magna inceptos id velit autem, harum phasellus quo. Officia congue, natoque imperdiet iusto malesuada placerat. Augue temporibus </p>
									<a href="#" class="learn_btn wow fadeInUp" data-wow-delay="0.4s">Learn More<i class="arrow_right"></i></a>
								</div>
							</div>

							<div class="col-lg-8">
								<div class="row">
									<div class="col-sm-6">
										<div class="media documentation_item wow fadeInUp">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/folder.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Working with Docs</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item wow fadeInUp" data-wow-delay="0.2s">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/envelope.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Formatting Content</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item wow fadeInUp" data-wow-delay="0.3s">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/smartphone.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Mobile Apps</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item wow fadeInUp" data-wow-delay="0.4s">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/management.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Account Management</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item wow fadeInUp" data-wow-delay="0.2s">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/newspaper.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Productivity</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item wow fadeInUp" data-wow-delay="0.4s">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/android.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Getting Started</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane documentation_tab_pane fade" id="local" role="tabpanel" aria-labelledby="local-tab">
						<div class="row">
							<div class="col-lg-4">
								<div class="documentation_text">
									<div class="round">
										<img src="<?=url("")?>assets-front/img/home_one/icon/file1.png" alt="">
									</div>

									<h4>Build Any Page In Seconds</h4>
									<p>Omnis voluptate magna inceptos id velit autem, harum phasellus quo. Officia congue, natoque imperdiet iusto malesuada placerat. Augue temporibus </p>
									<a href="#" class="learn_btn">Learn More<i class="arrow_right"></i></a>
								</div>
							</div>

							<div class="col-lg-8">
								<div class="row">
									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/smartphone.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Mobile Apps</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/management.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Account Management</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/folder.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Working with Docs</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/envelope.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Formatting Content</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/newspaper.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Productivity</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/android.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Getting Started</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane documentation_tab_pane fade" id="word" role="tabpanel" aria-labelledby="word-tab">
						<div class="row">
							<div class="col-lg-4">
								<div class="documentation_text">
									<div class="round">
										<img src="<?=url("")?>assets-front/img/home_one/icon/file1.png" alt="">
									</div>

									<h4>Build Any Page In Seconds</h4>
									<p>Omnis voluptate magna inceptos id velit autem, harum phasellus quo. Officia congue, natoque imperdiet iusto malesuada placerat. Augue temporibus </p>
									<a href="#" class="learn_btn">Learn More<i class="arrow_right"></i></a>
								</div>
							</div>

							<div class="col-lg-8">
								<div class="row">
									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/folder.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Working with Docs</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/envelope.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Formatting Content</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/smartphone.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Mobile Apps</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/management.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Account Management</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/newspaper.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Productivity</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/android.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Getting Started</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane documentation_tab_pane fade" id="wiki" role="tabpanel" aria-labelledby="wiki-tab">
						<div class="row">
							<div class="col-lg-4">
								<div class="documentation_text">
									<div class="round">
										<img src="<?=url("")?>assets-front/img/home_one/icon/file1.png" alt="">
									</div>

									<h4>Build Any Page In Seconds</h4>
									<p>Omnis voluptate magna inceptos id velit autem, harum phasellus quo. Officia congue, natoque imperdiet iusto malesuada placerat. Augue temporibus </p>
									<a href="#" class="learn_btn">Learn More<i class="arrow_right"></i></a>
								</div>
							</div>

							<div class="col-lg-8">
								<div class="row">
									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/folder.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Working with Docs</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/envelope.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Formatting Content</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/smartphone.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Mobile Apps</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/management.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Account Management</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/newspaper.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Productivity</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="media documentation_item">
											<div class="icon">
												<img src="<?=url("")?>assets-front/img/home_one/icon/android.png" alt="">
											</div>

											<div class="media-body">
												<a href="#">
													<h5>Getting Started</h5>
												</a>

												<p>He lost his bottle bubble and squeak knackered.!</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="doc_action_area parallaxie" data-background="img/bg.jpg" style="background: url(img/home_one/action_bg.jpg) no-repeat scroll;">
			<div class="overlay_bg"></div>
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-9 col-sm-8">
						<div class="action_text">
							<h2>Con't find an answer?</h2>
							<p>Head over to our Discord channel</p>
						</div>
					</div>

					<div class="col-lg-3 col-sm-4">
						<a href="#" class="action_btn">Discord Channel <i class="arrow_right"></i></a>
					</div>
				</div>
			</div>
		</section>

        <!-- For Cloning Recipient HTML -->
        <div id="recipient_clone" style="display: none;">
            <div class="clone_row">
                <a href="#" class="alert alert-danger remove_recipient">
                    <i class="icon_close_alt2"></i>
                </a>

                <div class="row">
                    <div class="col-sm-2 col-md-2 col-lg-2 form-group">
                        <input type="number" step="1" min="1" name="recipient_order[]" class="recipient_order form-control" value="1" readonly>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                        <label>Name</label>
                        <input type="text" name="recipient_name[]" class="recipient_name form-control" placeholder="Name">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                        <label>Email</label>
                        <input type="email" name="recipient_email[]" class="recipient_email form-control" placeholder="Email Address">
                    </div>
                </div>
            </div>
        </div>
        <!-- For Cloning Recipient HTML -->

		<!-- Footer start -->
		{{ view("includes-front/footer"); }}

        <!--=+=+=+=+=+=+=+=+=+= MODALS SECTION =+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+==+=+=+=+=+=+=+=+=+=-->

        <!-- Add Recipients Modal Start -->
        <div id="addRecipientsModal" class="modal addMoreRecipientsModal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Add Recipients</h4>
                </div>

                <div class="modal-body">
                    <form action="#" method="post" id="addRecipientsFrm">
                        <input type="hidden" name="csrf-token" value="{{ csrf_token() }}" />

                        <div class="row" style="position: relative; border-bottom: 1px solid #ccc;margin-bottom:12px">
                            <div class="col-sm-12 col-md-12 col-lg-12 cloneColDiv">
                                <div class="clone_row">

                                    <div class="row">
                                        <div class="col-sm-2 col-md-2 col-lg-2 form-group">
                                            <input type="number" step="1" min="1" name="recipient_order[]" class="recipient_order form-control" value="1" readonly>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                                            <label>Name *</label>
                                            <input type="text" name="recipient_name[]" class="recipient_name form-control" placeholder="Name">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                                            <label>Email *</label>
                                            <input type="email" name="recipient_email[]" class="recipient_email form-control" placeholder="Email Address">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-12 col-lg-12 text-right">
                                <a class="btn btn-primary add_another_recipient">Add Another</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <label>Subject</label>
                                <input type="text" name="recipient_subject" class="recipient_subject form-control" placeholder="Subject">
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                                <label>Message</label>
                                <textarea name="recipient_msg" class="recipient_msg form-control" placeholder="Message"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 mb-3">
                                <input type="submit" class="btn btn-success" id="addRecipientsBtn" value="Submit">
                            </div>

                        </div>
                    </form>

                    <div class="modal-footer">
<!--                        <button class="close" type="button">Close</button>-->
                        <button type="button" class="btn btn-danger" id="close">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Recipients Modal End -->

        <!--=+=+=+=+=+=+=+=+=+= MODALS SECTION =+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+==+=+=+=+=+=+=+=+=+=-->

	</div>
    <!--** Body Wrapper End **-->

	<!-- Back to top button -->
	<a id="back-to-top" title="Back to Top"></a>

	<!-- Footer scripts start -->
	{{ view("includes-front/footer-scripts"); }}

    <!-- Custom scripts start -->
    <script src="<?= url("") ?>assets-front/js/views/home.js"></script>
    <!-- Custom scripts end -->

	<script src="<?=url("");?>assets/js/simcify.min.js"></script>
</body>
</html>