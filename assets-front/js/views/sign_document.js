/*
 * When user accept the Document
 * Then show them Coordinates values
 */
$(".home_next_step").click(function() {
    var doc_key = $(this).data('doc_key');
    var fields = $(this).data('fields');
    var parent = $(this).parent();
    var total_fields = parent.find('.options_total span').text();

    if(doc_key != '') {
        $.ajax({
            url: '/home/get_fields',   // sending ajax request to this url
            type: 'get',
            data: {'doc_key':doc_key,"csrf-token": Cookies.get("CSRF-TOKEN")},
            dataType: "json",
            success: function(response) {
                // var doc_fields = JSON.stringify(response[fields]);
                var doc_options = response['doc_options'];
                var fields_length = response['options_count'];
                var last_option = response['last_option'];

                var doc_fields = doc_options[fields];
                if(typeof doc_fields === "undefined") {
                    return false;
                }

                var page_num = $('.signer-document .document-pagination .text-muted #page_num').text();
                var page_count = $('.signer-document .document-pagination .text-muted #page_count').text();

                $('.signer-overlay-previewer .signHere').remove();
                $('.signer-overlay-previewer .inputHere').remove();

                var page = '';
                var xPos = '';
                var yPos = '';
                var width = '';
                var height = '';
                var text = '';
                var fontsize = '';
                var fontfamily = '';
                var color = '';
                if(doc_fields['type'] == 'image') {
                    page = doc_fields['page'];
                    if(page == page_num) {
                        xPos = doc_fields['xPos'] + 'px';
                        var yPos_amount = parseFloat(doc_fields['yPos']) + parseInt(30);
                        yPos = yPos_amount+"px";
                        width = doc_fields['width'] + 'px';
                        height = doc_fields['height'] + 'px';

                        $('.signer-overlay-previewer').prepend('<img src="/assets/images/signhere.png" class="signHere" style="position:absolute;left:' + xPos + ';top:' + yPos + ';width:' + width + ';height:' + height + '">');

                        $('.home_next_step').animate({top:yPos},600);
                        // $("body, html").animate({
                        //     scrollTop: $(".signer-overlay-previewer").offset().top
                        // }, 400);
                    } else {
                        alert('Move to next page');
                        $('.home_next_step').animate({top:20},600);
                        return false;
                    }
                } else if(doc_fields['type'] == 'text') {
                    page = doc_fields['page'];
                    if(page == page_num) {
                        xPos = doc_fields['xPos'] + 'px';
                        var yPos_amount = parseFloat(doc_fields['yPos']) + parseInt(30);
                        yPos = yPos_amount+"px";
                        width = doc_fields['width'] + 'px';
                        height = doc_fields['height'] + 'px';
                        text = doc_fields['text'];
                        fontsize = doc_fields['fontsize']+'px';
                        fontfamily = doc_fields['fontfamily'];
                        color = doc_fields['color'];

                        $('.signer-overlay-previewer').prepend('<div class="inputHere" style="position:absolute;left:' + xPos + ';top:' + yPos + ';width:' + width + ';height:' + height + ';border:1px solid #ccc;font-family:' + fontfamily + ';font-size:' + fontsize + ';color:' + color + '">' + text + '</div>');

                        $('.home_next_step').animate({top:yPos},600);
                        // $("body, html").animate({
                        //     scrollTop: $(".signer-overlay-previewer").offset().top
                        // }, 400);
                    } else {
                        alert('Move to next page');
                        $('.home_next_step').animate({top:20},600);
                        return false;
                    }
                } else {
                    // alert('Nothing');
                }

                var fields_num = parseInt(fields) + parseInt(1);
                $(".home_next_step").data('fields',fields_num);

                var show_fields = parseInt(total_fields) - parseInt(1);
                parent.find('.options_total span').text(show_fields);

                if(fields_length == fields_num) {
                    var last_index = doc_options[last_option];
                    var xPos = last_index['xPos'] + 'px';
                    var yPos_amount = parseFloat(doc_fields['yPos']) + parseInt(30);
                    var yPos = yPos_amount+"px";

                    $(".home_next_step").hide();
                    $(".home_finish_step").show();
                    $('.home_finish_step').animate({top:yPos},800);
                    // $("body, html").animate({
                    //     scrollTop: 500
                    // }, 400);
                }

            }
        });
    }
});
