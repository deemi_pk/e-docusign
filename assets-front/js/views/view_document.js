//===========>> For 'More' dropdown menu
$(".mainnav div").click(function() {
    $("ul").slideToggle();
    $("ul ul").css("display", "none");
    $(".mainnav .on").toggleClass("on");
});

$(".hasDD").click(function(e) {
    $(this)
        .find("> ul")
        .slideToggle();
    $(this)
        .find("> ul ul")
        .css("display", "none");
    $(this)
        .find("> ul li")
        .removeClass("on");
    $(this).toggleClass("on");
    e.stopPropagation();
});

//===========>> Open Login Modal
$('body').on('click','.requestToRecipient',function() {
    $('.homeLoginModal').show();
});

//===========>> Close Login Modal
$('body').on('click','.close_modal',function() {
    $('.homeLoginModal').hide();
});

//===========>> Show Stripe Modal Block
$('body').on('click','#showDocStripe',function() {
    // Validate Login Modal
    var email = $('#docLoginStripe #email').val();
    var validEmail =  /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,6})+$/;
    if(typeof email === "undefined" || email == '' || !validEmail.test(email)) {
        var emailMsg;
        $('#docLoginStripe .emailFieldErr').find('.fieldEmptyErr').remove();

        if(email == '') {
            emailMsg = 'Email is required';
        }  else if(!validEmail.test(email)) {
            emailMsg = 'Please enter valid email';
        } else {
            emailMsg = '';
        }

        $('#docLoginStripe')
            .find('.emailFieldErr')
            .append('<p class="fieldEmptyErr">'+emailMsg+'</p>');

        $('#docLoginStripe .emailFieldErr #email').addClass('txtFieldErr');
        $('#docLoginStripe .emailFieldErr .fieldEmptyErr').show();
        $('#docLoginStripe .emailFieldErr #email').focus();

        return false;
    } else {
        $('#docLoginStripe .emailFieldErr #email').removeClass('txtFieldErr');
        $('#docLoginStripe .emailFieldErr .fieldEmptyErr').remove();
    }

    var password = $('#docLoginStripe #password').val();
    if(typeof password === "undefined" || password == '') {
        var passwordMsg;
        $('#docLoginStripe .passFieldErr').find('.fieldEmptyErr').remove();

        if(password == '') {
            passwordMsg = 'Password is required';
        } else {
            passwordMsg = '';
        }

        $('#docLoginStripe')
            .find('.passFieldErr')
            .append('<p class="fieldEmptyErr">'+passwordMsg+'</p>');

        $('#docLoginStripe .passFieldErr #password').addClass('txtFieldErr');
        $('#docLoginStripe .passFieldErr .fieldEmptyErr').show();
        $('#docLoginStripe .passFieldErr #password').focus();

        return false;
    } else {
        $('#docLoginStripe .passFieldErr #password').removeClass('txtFieldErr');
        $('#docLoginStripe .passFieldErr .fieldEmptyErr').remove();
    }

    $('#divDocStripe').show();
});

//===========>> Integrate Select2
$("#to_recipients").select2();

//===========>> Get document of a recipient
$(document.body).on("change","#to_recipients",function(){
    var recipient = $(this).val();
    var doc_id = $(this).data('doc_id');
    var origin = window.location.origin;   // Returns base URL (https://example.com)

    $.ajax({
        url: '/get_recipient_doc',   // sending ajax request to this url
        type: 'get',
        data: {'recipient':recipient,"doc_id":doc_id,"csrf-token":Cookies.get("CSRF-TOKEN")},
        // dataType: "json",
        success: function (response) {
            window.location.href = origin+"/view_document/"+response;
        }
    });
});

//===========>> Get document's coordinates of a recipient
window.onload = function (){
    var url = window.location.href;
    var split_url = url.split('/');
    var docu_key = split_url[4];

    setTimeout(function() {
        $.ajax({
            url: '/get_recipient_doc_coords',   // sending ajax request to this url
            type: 'get',
            data: {'document_key':docu_key,"csrf-token":Cookies.get("CSRF-TOKEN")},
            dataType: "json",
            success: function (response) {
                // var coordinates = JSON.stringify(response);

                if(response.length > 0) {
                    var page = '';
                    var xPos = '';
                    var yPos = '';
                    var width = '';
                    var height = '';
                    var text = '';
                    var fontsize = '';
                    var fontfamily = '';
                    var color = '';
                    $.each(response, function (i, coord) {
                        if (coord['type'] == 'image') {
                            xPos = coord['xPos'] + 'px';
                            // var yPos_amount = parseFloat(doc_fields['yPos']) + parseInt(30);
                            yPos = coord['yPos']+"px";
                            width = coord['width'] + 'px';
                            height = coord['height'] + 'px';

                            $('.e_docWrapper').prepend('<img src="/assets/images/signhere.png" class="signHere" style="position:absolute;left:'+xPos+';top:'+yPos+';width:'+width+';height:'+height+'">');
                        } else if (coord['type'] == 'text') {
                            xPos = coord['xPos'] + 'px';
                            // var yPos_amount = parseFloat(doc_fields['yPos']) + parseInt(30);
                            yPos = coord['yPos']+"px";
                            width = coord['width'] + 'px';
                            height = coord['height'] + 'px';
                            text = coord['text'];
                            fontsize = coord['fontsize']+'px';
                            fontfamily = coord['fontfamily'];
                            color = coord['color'];

                            $('.e_docWrapper').prepend('<div class="inputHere" style="position:absolute;left:'+xPos+';top:'+yPos+';width:'+width+';height:'+height+';border:1px solid #ccc;font-family:'+fontfamily+';font-size:'+fontsize+';color:'+color+';overflow-y:hidden">'+text+'</div>');
                        }
                    });
                }
            }

        });
    },1800);

};

//===========>> Stripe Payment
$(function() {
    var $form = $(".require-validation");

    $('form.require-validation').bind('submit', function(e) {

        var $form = $(".require-validation"),
            inputSelector = ['input[type=email]', 'input[type=password]',
                'input[type=text]', 'input[type=file]',
                'textarea'].join(', '),
            $inputs = $form.find('.required').find(inputSelector),
            $errorMessage = $form.find('div.error'),
            valid = true;
        $errorMessage.addClass('hide');

        $('.has-error').removeClass('has-error');
        $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('hide');
                e.preventDefault();
            }
        });

        if (!$form.data('cc-on-file')) {
            e.preventDefault();

            Stripe.setPublishableKey($form.data('stripe-publishable-key'));
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
        }

    });

    function stripeResponseHandler(status, response) {
        var total_payment = $('.stripePaymentModal .modal-body .total_payment .stripeTotalPayment').val();

        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            /* token contains id, last4, and card type */
            var token = response['id'];

            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");

            // $form.get(0).submit();
            $.ajax({
                type: "POST",
                url: '/stripe_payment',
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                data: {'response':response,'total_payment':total_payment}
            }).done(function( stripe_response ) {
                if(stripe_response['status'] == 'succeeded') {
                    $('#shipmentOrderForm').prepend('<input type="hidden" name="stripe_token" value="' + token + '"/>');
                    $("#shipmentOrderForm").submit();
                    $('.stripePaymentModal').hide();

                    // $('#submitShipmentOrder').trigger('click');
                }
            });
        }
    }
});

