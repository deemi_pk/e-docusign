/**
 * Created by PhpStorm.
 * User: Nadeem Arshad
 * Date: 10/18/2020
 * Time: 4:20 PM
 */
// Script for uploading document
$('body').on('click','#uploadDoc',function() {
    var file_name = $('#upload_file_form').find('#file_name').val();
    var doc_file = $('#upload_file_form').find('#doc_file').val();

    $.ajax({
        url: '/check_upload_document',   // sending ajax request to this url
        type: 'post',
        data: {'file_name': file_name, 'doc_file': doc_file, "csrf-token": Cookies.get("CSRF-TOKEN")},
        dataType: "json",
        success: function (response) {
            if(response['file_name'] == 'title_empty') {
                $('#upload_file_form .docNameErr .fieldEmptyErr').text('File name required');
                $('#upload_file_form .docNameErr .fieldEmptyErr').show();
                $('#upload_file_form .docNameErr input[type=text]').addClass('txtFieldErr');

                return false;
            }
            if(response['doc_file'] == 'doc_empty') {
                $('#upload_file_form .docFileErr .fieldEmptyErr').text('Document required');
                $('#upload_file_form .docFileErr .fieldEmptyErr').show();

                return false;
            }

            $('.addMoreRecipientsModal').show();
            // $('#upload_file_form').submit();
            return false;
        }

    });

    // // Check if user login or not
    // $.ajax({
    // 	url: '/check_user_login',   // sending ajax request to this url
    // 	type: 'post',
    // 	data: {"csrf-token": Cookies.get("CSRF-TOKEN")},
    // 	dataType: "json",
    // 	success: function (response) {
    // 		if(response['check'] == 'not_login') {
    // 			$('.homeLoginModal').show();
    //
    // 			return false;
    // 		} else if(response['check'] == 'login') {
    // 		    $('#upload_file_form').submit();
    //         }
    // 	}
    // });

    return false;
});

$('body').on('click','.add_another_recipient',function() {
    var last_recipient_name = $(".cloneColDiv .clone_row:last-child").find('.recipient_name').val();
    var last_recipient_email = $(".cloneColDiv .clone_row:last-child").find('.recipient_email').val();
    var validEmail =  /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,6})+$/;

    var name_msg = '';
    var check_name = true;
    if(last_recipient_name == '') {
        name_msg = 'Recipient name is required';

        check_name = false;
    } else if(last_recipient_name.match(/\s+(\W)/g)) {
        name_msg = 'Space not required';

        check_name = false;
    } else if(last_recipient_name.length < 3) {
        name_msg = 'Name must contains 3 characters';

        check_name = false;
    }

    if(check_name == false) {
        $(".cloneColDiv .clone_row:last-child")
            .find('.recipient_name')
            .next().remove('.fieldEmptyErr');

        $(".cloneColDiv .clone_row:last-child")
            .find('.recipient_name')
            .after('<p class="fieldEmptyErr" style="display:block;margin-bottom:0;">'+name_msg+'</p>');

        $(".cloneColDiv .clone_row:last-child")
            .find('.recipient_name').focus();

        return false;
    } else {
        $(".cloneColDiv .clone_row:last-child")
            .find('.recipient_name')
            .next().remove('.fieldEmptyErr');
        // return true;
    }

    var email_msg = '';
    var check_email = true;
    if(last_recipient_email == '') {
        email_msg = 'Recipient Email is required';

        check_email = false;
    } else if(!validEmail.test(last_recipient_email)) {
        email_msg = 'Valid email required';

        check_email = false;
    }

    if(check_email == false) {
        $(".cloneColDiv .clone_row:last-child")
            .find('.recipient_email')
            .next().remove('.fieldEmptyErr');

        $(".cloneColDiv .clone_row:last-child")
            .find('.recipient_email')
            .after('<p class="fieldEmptyErr" style="display:block;margin-bottom:0;">'+email_msg+'</p>');

        $(".cloneColDiv .clone_row:last-child")
            .find('.recipient_email').focus();

        return false;
    } else {
        $(".cloneColDiv .clone_row:last-child")
            .find('.recipient_email')
            .next().remove('.fieldEmptyErr');
        // return true;
    }

    var row_length = $('.cloneColDiv .clone_row').length;
    var order = parseInt(row_length) + parseInt(1);

    $(".cloneColDiv").append($("#recipient_clone").html());
    $('.cloneColDiv .clone_row .recipient_order').removeAttr('readonly');
    $(".cloneColDiv .clone_row:last-child").find('.recipient_order').val(order);
});

$('body').on('blur','.recipient_order',function() {
    var order_val = $(this).val();
    if((parseInt(order_val) == parseInt(0)) || (parseInt(order_val) < parseInt(0))) {
        alert('Order number must be greater then 0');

        $('.add_another_recipient').hide();
        $('#addRecipientsBtn').hide();
        return false;
    } else {
        $('.add_another_recipient').show();
        $('#addRecipientsBtn').show();

        return true;
    }
});

$('body').on('click', '.remove_recipient', function() {
    var parent = $(this).parent();

    if(confirm("Are you sure you want to delete this recipient?")) {
        parent.remove();
    } else {
        return false;
    }
});

$('body').on('click','#addRecipientsBtn',function() {
    // for array input values
    var recipient_order = $("#addRecipientsFrm input[name='recipient_order[]']").map(
        function(){
            return $(this).val();
        }).get();
    var recipient_name = $("#addRecipientsFrm input[name='recipient_name[]']").map(
        function(){
            return $(this).val();
        }).get();
    var recipient_email = $("#addRecipientsFrm input[name='recipient_email[]']").map(
        function(){
            return $(this).val();
        }).get();

    var recipient_subject = $("#addRecipientsFrm .recipient_subject").val();
    var recipient_msg = $("#addRecipientsFrm .recipient_msg").val();

    // if(check_name == true) {
        // // Check if user login or not
        $.ajax({
            url: '/set_recipient_session',   // sending ajax request to this url
            type: 'post',
            data: {
                "recipient_order": recipient_order,
                "recipient_name": recipient_name,
                "recipient_email": recipient_email,
                "recipient_subject": recipient_subject,
                "recipient_msg": recipient_msg,
                "csrf-token": Cookies.get("CSRF-TOKEN")
            },
            dataType: "json",
            success: function (response) {
                if (response) {
                    $('#upload_file_form').submit();
                }
            }
        });
    // } else {
    //     return false;
    // }
    return false;
});

// Script for login
$('body').on('click','#userLoginBtn',function() {
    var user_email = $('#userLoginFrm').find('#email').val();
    var user_password = $('#userLoginFrm').find('#password').val();

    $.ajax({
        url: '/user_login',   // sending ajax request to this url
        type: 'post',
        data: {"user_email":user_email,"user_password":user_password,"csrf-token": Cookies.get("CSRF-TOKEN")},
        dataType: "json",
        success: function (response) {
            if(response['email_check'] == 'email_empty') {
                $('#userLoginFrm .emailFieldErr .fieldEmptyErr').text('Email is required');
                $('#userLoginFrm .emailFieldErr .fieldEmptyErr').show();
                $('#userLoginFrm .emailFieldErr #email').addClass('txtFieldErr');

                return false;
            }
            if(response['password_check'] == 'password_empty') {
                $('#userLoginFrm .passFieldErr .fieldEmptyErr').text('Password is required');
                $('#userLoginFrm .passFieldErr .fieldEmptyErr').show();
                $('#userLoginFrm .passFieldErr #password').addClass('txtFieldErr');

                return false;
            }

            if(response['return_login']['status'] == 'error') {
                $('#loginModal .message_alert.alert-danger').find('h5').empty().text(response['return_login']['title']);
                $('#loginModal .message_alert.alert-danger').find('p').empty().text(response['return_login']['message']);
                $('#loginModal .message_alert.alert-danger').show();

                return false;
            } else if(response['return_login']['status'] == 'success') {
                $('#upload_file_form').submit();
            }
        }
    });

    return false;
});

$('body').on('click','#close',function() {
    $('.modal').hide();
});