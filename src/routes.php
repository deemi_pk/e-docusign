<?php

use Simcify\Router;
use Simcify\Exceptions\Handler;
use Simcify\Middleware\Authenticate;
use Simcify\Middleware\RedirectIfAuthenticated;
use Pecee\Http\Middleware\BaseCsrfVerifier;

/**
 * ,------,
 * | NOTE | CSRF Tokens are checked on all PUT, POST and GET requests. It
 * '------' should be passed in a hidden field named "csrf-token" or a header
 *          (in the case of AJAX without credentials) called "X-CSRF-TOKEN"
 *  */ 
Router::csrfVerifier(new BaseCsrfVerifier());

// Router::group(['prefix' => '/signer'], function() {

    Router::group(['exceptionHandler' => Handler::class], function() {
		/**
		 *  Front pages
		 **/
		// Home Page
		Router::get('/', 'Home@index');
		// About Us Page
		Router::get('/about', 'Home@about');
		// Services Page
		Router::get('/services', 'Home@services');
		// Price Planes Page
		Router::get('/price_planes', 'Home@price_planes');
		// Contact Us Page
		Router::get('/contact', 'Home@contact');
		// Sign Up Page
		Router::get('/sign_up', 'Home@sign_up');
		// Upload Document
		Router::post('/upload_document', 'Home@upload_document');
		// Check Upload Document
		Router::post('/check_upload_document', 'Home@check_upload_document');
        // User Login from Home
        Router::post('/set_recipient_session', 'Home@set_recipient_session');
		// Check User Login
		Router::post('/check_user_login', 'Home@check_user_login');
        // User Login from Home
        Router::post('/user_login', 'Home@user_login');
		// View Document Page
		Router::get('/view_document/{document_key}', 'Home@view_document');
        // Sign Out
        Router::get('/logout', 'Home@logout');
        // Save document changes
        Router::post('/home/doc_sign', 'home@doc_sign');
        // Get recipient document
        Router::get('/get_recipient_doc', 'home@get_recipientDoc');
        // Get recipient document coordinates
        Router::get('/get_recipient_doc_coords', 'home@get_recipientDocCoords');
        // Send signing request to recipients
        Router::post('/send_sign_request', 'Home@send_signRequest');

        Router::get('/sign_document/{order}/{document_key}', 'Home@recipient_signDocument');


        Router::get('/home/get_fields', 'Home@get_fields');

        Router::post('/next_recipient_doc_sign', 'Home@nextRecipient_docSign');


        Router::group(['middleware' => [Simcify\Middleware\Authenticate::class, \Simcify\Middleware\FilterRequestParameters::class]], function() {

            /**
             *  login Required pages
             **/ 

            // Dashboard
            Router::get('/dashboard', 'Dashboard@get');
            Router::get('/dashboard', 'Dashboard@get');

            // Notifications
            Router::get('/notifications', 'Notification@get');
            Router::post('/notifications/read', 'Notification@read');
            Router::post('/notifications/count', 'Notification@count');
            Router::post('/notifications/delete', 'Notification@delete');


            // Documents
            Router::get('/documents', 'Document@get');
            Router::get('/document/{docId}/download', 'Document@download', ['as' => 'docId']);
            Router::get('/document/{document_key}', 'Document@open');
            Router::post('/documents/sign', 'Document@sign');
            Router::post('/documents/send', 'Document@send');
            Router::post('/documents/fetch', 'Document@fetch');
            Router::post('/documents/delete', 'Document@delete');
            Router::post('/documents/restore', 'Document@restore');
            Router::post('/documents/convert', 'Document@convert');
            Router::post('/documents/protect', 'Document@protect');
            Router::post('/documents/replace', 'Document@replace');
            Router::post('/documents/relocate', 'Document@relocate');
            Router::post('/documents/duplicate', 'Document@duplicate');
            Router::post('/documents/upload/file', 'Document@uploadfile');
            Router::post('/documents/update/file', 'Document@updatefile');
            Router::post('/documents/update/file/access', 'Document@updatefileaccess');
            Router::post('/documents/update/file/acess/view', 'Document@updatefileaccessview');
            Router::post('/documents/delete/file', 'Document@deletefile');
            Router::post('/documents/create/folder', 'Document@createfolder');
            Router::post('/documents/update/folder', 'Document@updatefolder');
            Router::post('/documents/update/folder/access', 'Document@updatefolderaccess');
            Router::post('/documents/update/folder/access/view', 'Document@updatefolderaccessview');
            Router::post('/documents/update/folder/protect', 'Document@updatefolderprotect');
            Router::post('/documents/update/folder/protect/view', 'Document@updatefolderprotectview');
            Router::post('/documents/delete/folder', 'Document@deletefolder');
            Router::post('/documents/import/dropbox', 'Document@dropboximport');
            Router::post('/documents/import/googledrive', 'Document@googledriveimport');
            Router::post('/documents/update/permissions', 'Document@permissions');
            Router::get('/documents/get_fields', 'Document@get_fields');

            // Templates
            Router::get('/templates', 'Template@get');
            Router::post('/templates/fetch', 'Template@fetch');
            Router::post('/templates/create', 'Template@create');
            Router::post('/templates/upload/file', 'Template@uploadfile');
            Router::post('/templates/import/dropbox', 'Template@dropboximport');
            Router::post('/templates/import/googledrive', 'Template@googledriveimport');

            // Chat
            Router::post('/chat/post', 'Chat@post');
            Router::post('/chat/fetch', 'Chat@fetch');

            // Fields
            Router::post('/field/save', 'Field@save');
            Router::post('/field/delete', 'Field@delete');

            // Requests
            Router::get('/requests', 'Request@get');
            Router::post('/requests/send', 'Request@send');
            Router::post('/requests/delete', 'Request@delete');
            Router::post('/requests/cancel', 'Request@cancel');
            Router::post('/requests/remind', 'Request@remind');
            Router::post('/requests/decline', 'Request@decline');

            // Chat
            Router::post('/signature/save', 'Signature@save');
            Router::post('/signature/save/upload', 'Signature@upload');
            Router::post('/signature/save/draw', 'Signature@draw');

            // Team
            Router::get('/team', 'Team@get');
            Router::post('/team/create', 'Team@create');
            Router::post('/team/update', 'Team@update');
            Router::post('/team/update/view', 'Team@updateview');
            Router::post('/team/delete', 'Team@delete');

            // Departments
            Router::get('/departments', 'Department@get');
            Router::post('/departments/create', 'Department@create');
            Router::post('/departments/update', 'Department@update');
            Router::post('/departments/update/view', 'Department@updateview');
            Router::post('/departments/delete', 'Department@delete');

            // customers
            Router::get('/customers', 'Customer@get');
            Router::post('/customers/create', 'Customer@create');
            Router::post('/customers/update', 'Customer@update');
            Router::post('/customers/update/view', 'Customer@updateview');
            Router::post('/customers/delete', 'Customer@delete');

            // Companies
            Router::get('/companies', 'Company@get');
            Router::post('/companies/update', 'Company@update');
            Router::post('/companies/update/view', 'Company@updateview');
            Router::post('/companies/delete', 'Company@delete');
            Router::post('/companies/create', 'Company@create');

            // users
            Router::get('/users', 'User@get');
            Router::post('/users/create', 'User@create');
            Router::post('/users/update', 'User@update');
            Router::post('/users/update/view', 'User@updateview');
            Router::post('/users/delete', 'User@delete');
            Router::post('/users/checkemail', 'User@checkemail');

            // settings
            Router::get('/settings', 'Settings@get');
            Router::post('/settings/update/profile', 'Settings@updateprofile');
            Router::post('/settings/update/company', 'Settings@updatecompany');
            Router::post('/settings/update/system', 'Settings@updatesystem');
            Router::post('/settings/update/reminders', 'Settings@updatereminders');
            Router::post('/settings/update/password', 'Settings@updatepassword');
            Router::post('/settings/synchronizeTimezone', 'Settings@synchronizeTimezone');

            // Auth
            Router::get('/signout', 'Auth@signout');

            // update
            Router::get('/update', 'Update@get');
            Router::post('/update/scan', 'Update@scan');
            

        });
            
        Router::group(['middleware' => [Simcify\Middleware\RedirectIfAuthenticated::class, \Simcify\Middleware\FilterRequestParameters::class]], function() {

            /**
             * No login Required pages
             **/ 
            Router::get('/signin', 'Auth@get');
            Router::post('/signin/validate', 'Auth@signin');
            Router::post('/forgot', 'Auth@forgot');
            Router::get('/reset/{token}', 'Auth@getreset', ['as' => 'token']);
            Router::post('/reset', 'Auth@reset');
            Router::post('/signup', 'Auth@signup');

        });

        Router::get('/404', function() {
            response()->httpCode(404);
            echo view();
        });
        
        Router::get('/mailopen', 'Guest@mailopen');
        Router::get('/view/{document_key}', 'Guest@open');
        Router::post('/guest/decline', 'Guest@decline');
        Router::post('/guest/sign', 'Guest@sign');
            
    });

// });
