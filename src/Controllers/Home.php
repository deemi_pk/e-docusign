<?php
namespace Simcify\Controllers;

use Simcify\Str;
use Simcify\HomeFile;
use Simcify\Auth;
use Simcify\Auth as Authenticate;
use Simcify\Database;
use Simcify\HomeSigner;
use Mailgun\Mailgun;

//require_once('vendor/autoload.php');

class Home {
	/**
	 * Get home view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function index() {
	    session_start();
        if(isset($_SESSION["session_ID"])){
            unset($_SESSION["session_ID"]);
        }
        // Removing session data
        if(isset($_SESSION["recipients"])){
            unset($_SESSION["recipients"]);
        }
        $user = Auth::user();

		return view('front/home',compact('user'));
	}

	/**
	 * Get about us view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function about() {
        $user = Auth::user();

        return view('front/about',compact('user'));
	}

	/**
	 * Get services view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function services() {
        $user = Auth::user();

        return view('front/services',compact('user'));
	}

	/**
	 * Get price planes view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function price_planes() {
        $user = Auth::user();

        return view('front/price_planes',compact('user'));
	}

	/**
	 * Get contact us view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function contact() {
        $user = Auth::user();

        return view('front/contact',compact('user'));
	}

	/**
	 * Get contact us view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function sign_up() {
		return view('front/sign_up');
	}

	/**
	 * Get contact us view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function sign_in() {
		return view('front/sign_in');
	}

	/**
	 * Upload Document
	 *
	 * @return \Pecee\Http\Response
	 */
	public function upload_document() {
        session_start();
//        $user = Auth::user();
        $recipients = $_SESSION['recipients'];
        $session_ID = $_SESSION["session_ID"];

        $actionTakenBy = "Guest";
        if(!empty($user)) {
            $actionTakenBy = escape($user->fname.' '.$user->lname);
        }

        /*
         * Check, whether IP address register is allowed in .env
         * If yes, then capture the user's IP address
         */
        if (env('REGISTER_IP_ADDRESS_IN_HISTORY') == 'Enabled') {
            $actionTakenBy .= ' ['.getUserIpAddr().']';
        }

		$doc_key = Str::random(32);
		$data = array(
			"name" => input("file_title"),
			"folder" => input("folder"),
			"file" => $_FILES['file'],
			"is_template" => "No",
			"source" => "form",
			"document_key" => $doc_key,
			"activity" => 'File uploaded by <span class="text-primary">'.$actionTakenBy.'</span>.'
		);
		$upload = HomeSigner::home_upload($data,$recipients);
//		die;

		if($upload['status'] == 'success') {
            // Removing session data
//            if(isset($_SESSION["recipients"])){
//                unset($_SESSION["recipients"]);
//            }

			redirect(env("APP_URL")."view_document/".$doc_key);
		}
	}

	/**
	 * Check Upload Document
	 *
	 * @return \Pecee\Http\Response
	 */
	public function check_upload_document() {
		$return_title = '';
		if(!isset($_POST['file_name']) || empty($_POST['file_name'])) {
			$return_title = 'title_empty';
		}
		$return_file = '';
		if(!isset($_POST['doc_file']) || empty($_POST['doc_file'])) {
			$return_file = 'doc_empty';
		}

		return json_encode(array('file_name'=>$return_title,'doc_file'=>$return_file));
	}

    /**
     * Set Recipients Session
     *
     * @return \Pecee\Http\Response
     */
	public function set_recipient_session() {
        session_start();
        $session_ID = Str::random(45);

        // Removing session data
        if(isset($_SESSION["session_ID"])){
            unset($_SESSION["session_ID"]);
        }
        // Removing session data
        if(isset($_SESSION["recipients"])){
            unset($_SESSION["recipients"]);
        }

	    $recipients_arr = array();
	    foreach($_POST['recipient_order'] as $key => $r_order) {
            $recipients_arr[] = array(
                'order'=>$r_order,
                'name'=>$_POST['recipient_name'][$key],
                'email'=>$_POST['recipient_email'][$key],
            );
        }
	    $recipients = array(
	        'session_ID'=>$session_ID,
            'recipients_detail'=>$recipients_arr,
            'subject'=>$_POST['recipient_subject'],
            'message'=>$_POST['recipient_msg']
        );

        $_SESSION['session_ID'] = $session_ID;
        $_SESSION['recipients'] = $recipients;

        return true;
    }

	/**
	 * Check User Login
	 *
	 * @return \Pecee\Http\Response
	 */
	public function check_user_login() {
		$user = Auth::user();

		$checkUser = '';
		if((count($user) > 0) || (!empty($user))) {
            $checkUser = 'login';
		} else {
			$checkUser = 'not_login';
		}

		$return = array('check'=>$checkUser);
		return json_encode($return);
	}

    /**
     * Sign In a user from home
     *
     * @return Json
     */
    public function user_login() {
        $return_check = true;
        $signIn = array();

        $email_check = '';
        if(!isset($_POST['user_email']) || empty($_POST['user_email'])) {
            $email_check = 'email_empty';

            $return_check = false;
        }
        $password_check = '';
        if(!isset($_POST['user_password']) || empty($_POST['user_password'])) {
            $password_check = 'password_empty';

            $return_check = false;
        }

        if($return_check == false) {
            return json_encode(array('email_check'=>$email_check,'password_check'=>$password_check,'return_login'=>$signIn));
        }

        $signIn = Authenticate::login(
            $_POST['user_email'],
            $_POST['user_password'],
            array(
                "rememberme" => true,
                "status" => "Active"
            )
        );

        if(!empty($signIn)) {
            return json_encode(array('email_check'=>$email_check,'password_check'=>$password_check,'return_login'=>$signIn));
        }

        //Sync the MySQL and PHP datetimes
//        Database::table('users')->synchronizeTimezone();

//        header('Content-type: application/json');
//        exit(json_encode($signIn));
    }

	/**
	 * Upload Document
	 *
	 * @return \Pecee\Http\Response
	 */
	public function view_document($document_key) {
	    session_start();
        $user = Auth::user();
        $recipients = $_SESSION["recipients"];
        $session_ID = $_SESSION["session_ID"];

//        $stripe = new \Stripe\StripeClient(env("STRIPE_SECRET"));
//        $charge = $stripe->charges->create([
//            'amount' => 2000,
//            'currency' => 'usd',
//            'source' => 'tok_amex', // obtained with Stripe.js
//            'description' => 'My First Test Charge (created for API docs)'
//        ]);

        $requestPositions = json_encode(array());
		$requestWidth = 0;

		$document = Database::table("documents")
			->where("document_key", $document_key)
			->first();

		if (empty($document)) {
			return view('errors/404');
		}

		if($document->origin == 0) {
		    $parent_doc_id = $document->id;
        } else {
            $parent_doc_id = $document->origin;
        }

		if ($document->is_template == "Yes") {
			$lauchLabel = "Manage Fields & Edit";

			$template_fields = json_decode($document->template_fields, true);
			$fields = array_slice($template_fields,1);
			$fields_count = count($fields);

			$savedWidth = $template_fields[0];
			if (empty($savedWidth)) {
				$savedWidth = 0;
			}
		} else {
			$lauchLabel = "Sign & Edit";

			$template_fields = json_encode(array());
			$template_fields_count = 0;
			$savedWidth = 0;
		}
		$templateFields = json_encode($template_fields, true);
//		echo "<pre>";
//		print_r($templateFields);
//		die;

        if (isset($_GET['signingKey'])) {
            $signingKey = $_GET['signingKey'];

            $request = Database::table("requests")
                ->where("signing_key", $signingKey)
                ->first();
            $actionTakenBy = escape($user->fname." - ".$request->email);

            /*
            * Check, whether IP address register is allowed in .env
            * If yes, then capture the user's IP address
            */
            if (env('REGISTER_IP_ADDRESS_IN_HISTORY') == 'Enabled') {
                $actionTakenBy .= ' ['.getUserIpAddr().']';
            }

            $activity = '<span class="text-primary">'.$actionTakenBy.'</span> opened this document.';
            HomeSigner::home_keephistory($request->document, $activity, "default");
            if (!empty($request->positions)) {
                $requestPositions = json_decode($request->positions, true);
                $requestWidth = $requestPositions[0];
                $requestPositions = json_encode($requestPositions, true);

                if (empty($requestWidth)) {
                    $requestWidth = 0;
                }
            }
        } else {
            $request = '';
        }

        $temp_recipients = Database::table("temp_recipients")
            ->where("session_id",$session_ID)
            ->orderBy("temp_order",true)
            ->get();
		$fields = Database::table("fields")
			->where("user", $user->id)
			->where("type", "custom")
			->orderBy("id", false)
			->get();
		$stamps = Database::table("fields")
			->where("user", $user->id)
			->where("type", "stamp")
			->orderBy("id", false)
			->get();
		$inputfields = Database::table("fields")
			->where("user", $user->id)
			->where("type", "input")
			->orderBy("id", false)
			->get();

//		echo "<pre>";
//		print_r($user);
//		die;

		return view('front/view_document', compact("document","templateFields","user","request","savedWidth","lauchLabel","fields","stamps","inputfields","recipients","temp_recipients","parent_doc_id","session_ID"));
	}

    /**
     * Sign & Edit Document
     *
     * @return Json
     */
    public function doc_sign() {
        header('Content-type: application/json');
        $sign = HomeSigner::home_sign(input("document_key"), input("actions"), input("docWidth"), input("signing_key"));

        if ($sign) {
            exit(json_encode(responder("success", "Alright!", "Document successfully saved.","reload()")));
        } else {
            exit(json_encode(responder("error", "Oops!", "Something went wrong, please try again.")));
        }
    }

    /**
     * Recipient Document
     * On change recipient
     *
     * Get selected recipient document
     *
     * @return Json
     */
    public function get_recipientDoc() {
        if(isset($_GET['recipient']) || !empty($_GET['recipient'])) {
            $recipient = $_GET['recipient'];
            $doc_id = $_GET['doc_id'];

            $document = Database::table("documents")
                ->where("origin",$doc_id)
                ->where("name","LIKE", "%".$recipient."%")
                ->where("is_template","Yes")
                ->first();

            $return = $document->document_key;
            return $return;
        }
    }

    /**
     * Recipient Document
     * Onload page
     *
     * Get selected recipient document coordinates
     *
     * @return Json
     */
    public function get_recipientDocCoords() {
        if(isset($_GET['document_key']) || !empty($_GET['document_key'])) {
            $document_key = $_GET['document_key'];

            $document = Database::table("documents")
                ->where("document_key", $document_key)
                ->first();

            if(!empty($document) && $document->is_template == 'Yes' && !empty($document->template_fields)) {
                $template_fields = json_decode($document->template_fields, true);
                $fields = array_slice($template_fields,1);

                return json_encode($fields);
            } else {
                return json_encode(array());
            }
        }
    }

    /**
     * Send signing request
     *
     * @return Json
     */
    public function send_signRequest() {
        session_start();
        $user = Auth::user();

        $actionTakenBy = "Guest";
        if(!empty($user)) {
            $actionTakenBy = escape($user->fname.' '.$user->lname);
        }
        $recipients = $_SESSION["recipients"];
        $session_ID = $_SESSION["session_ID"];
//        echo "<pre>";
//        print_r($recipients);
//        echo "\n";
//        die;

        /*
         * Check, whether IP address register is allowed in .env
         * If yes, then capture the user's IP address
         */
        if (env('REGISTER_IP_ADDRESS_IN_HISTORY') == 'Enabled') {
            $actionTakenBy .= ' ['.getUserIpAddr().']';
        }

        $requestByOrder = 1;
        $_recipients = Database::table("temp_recipients")
            ->where("session_id",$session_ID)
            ->where("temp_order",$requestByOrder)
            ->first();
//        echo "<pre>";
//        print_r($_recipients);
//        die;

        if(!empty($_recipients)) {
//            foreach($_recipients as $recipient) {
                $signingKey = Str::random(32);

                $document = Database::table("documents")
                    ->where("id",$_recipients->document_id)
                    ->first();

//                echo "<pre>";
//                print_r($document);
//                die;

                if(!empty($document) && $document->is_template == 'Yes' && !empty($document->template_fields)) {
                    $duplicateActivity = 'Signing request sent to <span class="text-primary">'.escape($_recipients->email).'</span> by <span class="text-primary">'.$actionTakenBy.'</span>.';
                    $send_mail = HomeSigner::home_keephistory($document->document_key,$duplicateActivity,"default");
//                    echo $send_mail;
//                    echo "\n";

//                    if( env('GUEST_SIGNING') == "Enabled" AND env('FORCE_GUEST_SIGNING') == "Enabled") {
//                        $signingLink = env("APP_URL")."view/".$document->document_key."?signingKey=".$signingKey;
//                    } else {
//                        $signingLink = env("APP_URL")."document/".$document->document_key."?signingKey=".$signingKey;
//                    }

                    $signingLink = env("APP_URL")."sign_document/".$requestByOrder."/".$document->document_key."?signingKey=".$signingKey;
//                    echo "Sign Link => ".$signingLink;
//                    echo "\n";
                    $trackerLink = env("APP_URL")."mailopen?signingKey=".$signingKey;
//                    echo "Track Link => ".$trackerLink;
//                    echo "\n";

                    $request = array(
                        "signing_key" => $signingKey,
                        "document" => $document->document_key,
                        "receiver_email" => $_recipients->email,
                        "sender_note"=>escape($_recipients->message),
                    );
                    $request = Database::table("recipients_request")->insert($request);
//                    echo "Request Sent => " . $request;
//                    echo "\n";

                    // First, instantiate the SDK with your API credentials
                    $mg = Mailgun::create('2b29bf635fa4a1c3f63179909e5f15cf-52b6835e-6fe15f4e'); // For US servers
                    //
                    // Now, compose and send your message.
                    $mg->messages()->send('mail.mydoctionary.com', [
                        'from'    => 'optimusprime.pk@gmail.com',
                        'to'      => $_recipients->email,
                        'subject' => $_recipients->subject,
                        'text'    => 'We send you document. <a href="'.$signingLink.'">Click Here</a>',
                        //            'attachment' => [[
                        //                'filePath' => config("app.storage")."files/".$document->filename,
                        //                'filename' => $document->name.'.'.$document->extension
                        //
                        //            ]]
                    ]);
//                    echo "<pre>";
//                    print_r($mg);
//                    echo "\n";

//                    $delete = Database::table("temp_recipients")
//                        ->where("session_id",$session_ID)
//                        ->delete();
//                    echo "Delete Temp => ".$delete;
//                    echo "\n";
                }
//            }

//            unset($_SESSION["session_ID"]);
//            unset($_SESSION["recipients"]);
        }
//        die;

        redirect(env("APP_URL"));
    }

    public function recipient_signDocument($order,$document_key) {
        $user = Auth::user();

        $document = Database::table("documents")
            ->where("document_key", $document_key)
            ->leftJoin("temp_recipients","temp_recipients.document_id","documents.id")
            ->first();
        $requests = Database::table("recipients_request")
            ->where("document", $document_key)
            ->first();

        if(empty($user)) {
            $user_name = $document->name;
        } else {
            $user_name = $user->fname;
        }

        if (empty($document)) {
            return view('errors/404');
        }

        if ($document->is_template == "Yes") {
            $lauchLabel = "Manage Fields & Edit";

            $template_fields = json_decode($document->template_fields, true);
            $fields = array_slice($template_fields,1);
            $fields_count = count($fields);

            $savedWidth = $template_fields[0];
            if (empty($savedWidth)) {
                $savedWidth = 0;
            }
        } else {
            $lauchLabel = "Sign & Edit";

            $template_fields = json_encode(array());
            $template_fields_count = 0;
            $savedWidth = 0;
        }

        if (isset($_GET['signingKey'])) {
            $signingKey = $_GET['signingKey'];

            $request = Database::table("recipients_request")
                ->where("signing_key",$signingKey)
                ->first();
            $actionTakenBy = escape($user_name." - ".$request->receiver_email);

            /*
            * Check, whether IP address register is allowed in .env
            * If yes, then capture the user's IP address
            */
            if (env('REGISTER_IP_ADDRESS_IN_HISTORY') == 'Enabled') {
                $actionTakenBy .= ' ['.getUserIpAddr().']';
            }

            $activity = '<span class="text-primary">'.$actionTakenBy.'</span> opened this document.';
            HomeSigner::home_keephistory($request->document, $activity, "default");
        } else {
            echo "Else";
            $request = '';
        }

        $templateFields = json_encode($template_fields, true);

        $fields = Database::table("fields")
            ->where("user", $user->id)
            ->where("type", "custom")
            ->orderBy("id", false)
            ->get();
        $stamps = Database::table("fields")
            ->where("user", $user->id)
            ->where("type", "stamp")
            ->orderBy("id", false)
            ->get();
        $inputfields = Database::table("fields")
            ->where("user", $user->id)
            ->where("type", "input")
            ->orderBy("id", false)
            ->get();

        return view('front/sign_document', compact("user", "document","order","fields","inputfields","templateFields","savedWidth","lauchLabel","request","requestWidth","stamps","fields_count"));
    }

    public function get_fields() {
        $user = Auth::user();
        $doc_key = $_GET['doc_key'];

        $document = Database::table("documents")
            ->where("document_key", $doc_key)
            ->first();
        $json_decode = json_decode($document->template_fields,true);

        $doc_options = array_slice($json_decode,1);
        $options_count = count($doc_options);
        $last_option = key(array_slice($doc_options, -1, 1, true));

        $doc_inputs = array('doc_options'=>$doc_options,'options_count'=>$options_count,'last_option'=>$last_option);

        return json_encode($doc_inputs);
    }

    public function nextRecipient_docSign() {
        header('Content-type: application/json');

        $document = Database::table("documents")
            ->where("document_key", input("document_key"))
            ->leftJoin("temp_recipients","temp_recipients.document_id","documents.id")
            ->first();

        $session_id = $document->session_id;
        $requestByOrder = $document->temp_order + 1;

        $sign = HomeSigner::home_sign(input("document_key"), input("actions"), input("docWidth"), input("signing_key"));

        if ($sign) {

            session_start();
            $user = Auth::user();

            $actionTakenBy = "Guest";
            if(!empty($user)) {
                $actionTakenBy = escape($user->fname.' '.$user->lname);
            }
//        echo "<pre>";
//        print_r($recipients);
//        echo "\n";
//        die;

            /*
             * Check, whether IP address register is allowed in .env
             * If yes, then capture the user's IP address
             */
            if (env('REGISTER_IP_ADDRESS_IN_HISTORY') == 'Enabled') {
                $actionTakenBy .= ' ['.getUserIpAddr().']';
            }

            $_recipients = Database::table("temp_recipients")
                ->where("session_id",$session_id)
                ->where("temp_order",$requestByOrder)
                ->first();

            if(!empty($_recipients)) {
//            foreach($_recipients as $recipient) {
                $signingKey = Str::random(32);

                $document = Database::table("documents")
                    ->where("id",$_recipients->document_id)
                    ->first();

                if(!empty($document) && $document->is_template == 'Yes' && !empty($document->template_fields)) {
                    $duplicateActivity = 'Signing request sent to <span class="text-primary">'.escape($_recipients->email).'</span> by <span class="text-primary">'.$actionTakenBy.'</span>.';
                    $send_mail = HomeSigner::home_keephistory($document->document_key,$duplicateActivity,"default");
//                    echo $send_mail;
//                    echo "\n";

//                    if( env('GUEST_SIGNING') == "Enabled" AND env('FORCE_GUEST_SIGNING') == "Enabled") {
//                        $signingLink = env("APP_URL")."view/".$document->document_key."?signingKey=".$signingKey;
//                    } else {
//                        $signingLink = env("APP_URL")."document/".$document->document_key."?signingKey=".$signingKey;
//                    }

                    $signingLink = env("APP_URL")."sign_document/".$requestByOrder."/".$document->document_key."?signingKey=".$signingKey;
//                    echo "Sign Link => ".$signingLink;
//                    echo "\n";
                    $trackerLink = env("APP_URL")."mailopen?signingKey=".$signingKey;
//                    echo "Track Link => ".$trackerLink;
//                    echo "\n";

                    $request = array(
                        "signing_key" => $signingKey,
                        "document" => $document->document_key,
                        "receiver_email" => $_recipients->email,
                        "sender_note"=>escape($_recipients->message),
                    );
                    $request = Database::table("recipients_request")->insert($request);

//                    echo $request;
//                    die;

                    // First, instantiate the SDK with your API credentials
                    $mg = Mailgun::create('2b29bf635fa4a1c3f63179909e5f15cf-52b6835e-6fe15f4e'); // For US servers
                    //
                    // Now, compose and send your message.
                    $mg->messages()->send('mail.mydoctionary.com', [
                        'from'    => 'optimusprime.pk@gmail.com',
                        'to'      => $_recipients->email,
                        'subject' => $_recipients->subject,
                        'text'    => 'We send you document. <a href="'.$signingLink.'">Click Here</a>',
                        //            'attachment' => [[
                        //                'filePath' => config("app.storage")."files/".$document->filename,
                        //                'filename' => $document->name.'.'.$document->extension
                        //
                        //            ]]
                    ]);
//                    echo "<pre>";
//                    print_r($mg);
//                    echo "\n";

//                    $delete = Database::table("temp_recipients")
//                        ->where("session_id",$session_ID)
//                        ->delete();
//                    echo "Delete Temp => ".$delete;
//                    echo "\n";
                }
//            }

//            unset($_SESSION["session_ID"]);
//            unset($_SESSION["recipients"]);
            }
//        die;

            exit(json_encode(responder("success", "Alright!", "Document successfully saved.","reload()")));

        } else {
            exit(json_encode(responder("error", "Oops!", "Something went wrong, please try again.")));
        }
    }

    /**
     * Sign Out a logged in user
     *
     */
    public function logout() {
        Authenticate::deauthenticate();
        redirect(url("Home@index"));
    }
}