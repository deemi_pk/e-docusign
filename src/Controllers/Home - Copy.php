<?php
namespace Simcify\Controllers;

use Simcify\Str;
use Simcify\File;
use Simcify\Auth;
use Simcify\Database;
use Simcify\Signer;
use Mailgun\Mailgun;

class Home {
	/**
	 * Get home view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function index() {
		return view('front/home');
	}

	/**
	 * Get about us view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function about() {
		return view('front/about');
	}

	/**
	 * Get services view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function services() {
		return view('front/services');
	}

	/**
	 * Get price planes view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function price_planes() {
		return view('front/price_planes');
	}

	/**
	 * Get contact us view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function contact() {
		return view('front/contact');
	}

	/**
	 * Get contact us view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function sign_up() {
		return view('front/sign_up');
	}

	/**
	 * Get contact us view
	 *
	 * @return \Pecee\Http\Response
	 */
	public function sign_in() {
		return view('front/sign_in');
	}

	/**
	 * Upload Document
	 *
	 * @return \Pecee\Http\Response
	 */
	public function upload_document() {
		$data = array(
			"name" => input("file_title"),
			"folder" => input("folder"),
			"file" => $_FILES['file'],
			"is_template" => "No",
			"source" => "form",
			"document_key" => Str::random(32),
			"activity" => 'File uploaded by <span class="text-primary">Guest</span>.'
		);
		$upload = Signer::home_upload($data);

		if($upload['status'] == 'success') {
			redirect(env("APP_URL")."view_document/".Str::random(32));
		}
	}

	/**
	 * Upload Document
	 *
	 * @return \Pecee\Http\Response
	 */
	public function view_document($document_key) {
		echo "View Document => ".$document_key;
	}
}