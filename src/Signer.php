<?php 
namespace Simcify;

require_once 'TCPDF/tcpdf.php';
require_once 'TCPDF/tcpdi.php';
use TCPDF;
use TCPDI;
use Simcify\Str;
use Simcify\File;
use Simcify\Auth;
use Simcify\Database;
use \CloudConvert\Api;

class PDF extends TCPDI {
    var $_tplIdx;
    var $numPages;

    function Header() {}

    function Footer() {}

}

class Signer {
    
    /**
     * Upload file
     * 
     * @param   array $data
     * @return  true
     */
    public static function upload($data) {
        $user = Auth::user();

        // get usage data
        if ($user->role == "user") {
            $fileUsage = Database::table("files")
                ->where("uploaded_by" , $user->id)
                ->count("id", "files")[0]->files;

            $diskUsage = Database::table("files")
                    ->where("uploaded_by" , $user->id)
                    ->sum("id", "size")[0]->size / 1000;

            // check file usage limits
            if ($fileUsage > env("PERSONAL_FILE_LIMIT")) {
                return responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("PERSONAL_FILE_LIMIT")." files.");
            }

            // check disk usage limits
            if ($diskUsage > env("PERSONAL_DISK_LIMIT")) {
                return responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("PERSONAL_DISK_LIMIT")." MBs.");
            }
        } else {
            $fileUsage = Database::table("files")
                ->where("company" , $user->company)
                ->count("id", "files")[0]->files;

            $diskUsage = Database::table("files")
                    ->where("company" , $user->company)
                    ->sum("id", "size")[0]->size / 1000;

            // check file usage limits
            if ($fileUsage > env("BUSINESS_FILE_LIMIT")) {
                return responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("BUSINESS_FILE_LIMIT")." files.");
            }

            // check disk usage limits
            if ($diskUsage > env("BUSINESS_DISK_LIMIT")) {
                return responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("BUSINESS_DISK_LIMIT")." MBs.");
            }
        }

        if ($user->company == 0) {
            $files = Database::table("files")
                ->where("name", $data["name"])
                ->where("folder", $data["folder"])
                ->first();
        } else {
            $files = Database::table("files")
                ->where("name", $data["name"])
                ->where("folder", $data["folder"])
                ->where("company", $user->company)
                ->first();
        }

        if (!empty($files) && $data["source"] == "form") {
            return responder("error", "Already Exists!", "File name '".$data["name"]."' already exists.");
        }

        if(env("ALLOW_NON_PDF") == "Enabled") {
            $allowedExtensions = "pdf, doc, docx, ppt, pptx, xls, xlsx";
        } else {
            $allowedExtensions = "pdf";
        }

        if ($data['source'] == "googledrive") {
            $upload = array(
                "status" => "success",
                "info" => array(
                    "name" => $data['file'],
                    "size" => $data['size'],
                    "extension" => "pdf"
                )
            );
        } else {
            $upload = File::upload (
                $data['file'], 
        "files",
                array(
                    "source" => $data['source'],
                    "allowedExtensions" => $allowedExtensions
                )
            );
        }

        if ($upload['status'] == "success") {
            self::keepcopy($upload['info']['name']);

            $data["filename"] = $upload['info']['name'];
            $data["size"] = $upload['info']['size'];
            $data["extension"] = $upload['info']['extension'];
            $activity = $data['activity'];
            unset($data['file'], $data['source'], $data['activity']);

            Database::table("files")->insert($data);
            $documentId = Database::table("files")->insertId();
            $document = Database::table("files")->where("id", $documentId)->get("document_key");

            Database::table("history")
                ->insert(array("company" => $data['company'], "file" => $document[0]->document_key, "activity" => $activity, "type" => "default"));

            return responder("success", "Upload Complete", "File successfully uploaded.");
        } else {
            return responder("error", "Oops!", $upload['message']);
        }
    }

    /**
     * Duplicate file
     * 
     * @param   int $data
     * @return  true
     */
    public static function duplicate($file, $duplicateName = '') {
        $document = Database::table("files")->where("id", $file)->first();

    	$user = Auth::user();
        $fileName = Str::random(32).".".$document->extension;
        copy(config("app.storage")."files/".$document->filename, config("app.storage")."files/".$fileName);
        self::keepcopy($fileName);

        if(empty($duplicateName)) {
            $duplicateName = $document->name." (Copy)";
        }
        $activity = 'File Duplicated from '.escape($document->name).' by <span class="text-primary">'.escape($user->fname.' '.$user->lname).'</span>.';

        if(!empty($duplicateName)) {
            $template_fields = $document->template_fields;
        } else  {
            $template_fields = null;
        }
        $data = array(
            "company" => $user->company,
            "uploaded_by" => $user->id,
            "name" => $duplicateName,
            "folder" => $document->folder,
            "filename" => $fileName,
            "extension" => $document->extension,
            "size" => $document->size,
            "status" => $document->status,
            "is_template" => $document->is_template,
            "template_fields" => $template_fields,
            "parent" => $file,
            "document_key" => Str::random(32)
        );
        Database::table("files")->insert($data);

        $documentId = Database::table("files")->insertId();
        $document = Database::table("files")->where("id", $documentId)->get("document_key");
        Database::table("history")
            ->insert(array("company" => $data['company'], "file" => $document[0]->document_key, "activity" => $activity, "type" => "default"));

        return $documentId;
    }
    
    /**
     * Copy file
     * 
     * @param   string $filename
     * @return  true
     */
    public static function keepcopy($filename) {
        copy(config("app.storage")."files/".$filename, config("app.storage")."copies/".$filename);

        return true;
    }
    
    /**
     * Copy file
     * 
     * @param   string $filename
     * @return  true
     */
    public static function renamecopy($fileName, $newName) {
        rename(config("app.storage")."copies/".$fileName, config("app.storage")."copies/".$newName);

        return true;
    }
    
    /**
     * Delete a folder
     * 
     * @param   string|int $folderId
     * @return  true
     */
    public static function deletefolder($folderId) {
        $foldersToDelete = $filesToDelete = array();
        $user = Auth::user();

        $thisFolder = Database::table("folders")->where("id", $folderId)->first();
        if ($user->company != $thisFolder->company) {
            return false;
        }

        $folders = Database::table("folders")
            ->where("id", $folderId)
            ->get();
        foreach ($folders as $folder) {
            $foldersToDelete[] = $folder->id;

            $folderFiles = Database::table("files")->where("folder", $folder->id)->get();
            foreach ($folderFiles as $file) {
                self::deletefile($file->filename, true);
            }

            //self::deletefolder($folder->id);
            Database::table("folders")->where("id", $folder->id)->delete();
        }
        $folderFiles = Database::table("files")->where("folder", $folderId)->get();

        foreach ($folderFiles as $file) {
            self::deletefile($file->filename, true);
        }
        Database::table("folders")->where("id", $folderId)->delete();

        return true;
    }
    
    /**
     * Delete a file
     * 
     * @param   int $fileId
     * @return  true
     */
    public static function deletefile($fileId, $actualFile = false) {
        if (!$actualFile) {
            $user = Auth::user();

            $thisFile = Database::table("files")->where("id", $fileId)->first();
            if ($user->company != $thisFile->company) {
                return false;
            } else if($user->role = "user" AND $thisFile->uploaded_by != $user->id) {
                $hiddenFiles = $user->hiddenfiles;

                if(empty($user->hiddenfiles)) {
                    $hiddenFiles = array($thisFile->id);
                } else {
                    $hiddenFilesArray = json_decode($hiddenFiles);
                    $hiddenFiles = array_push($hiddenFiles, $thisFile->id);
                }

                Database::table("users")
                    ->where("id", $user->id)
                    ->update(array("hiddenfiles" => json_encode($hiddenFiles)));
            } else {
                File::delete($thisFile->filename, "files");
                File::delete($thisFile->filename, "copies");

                Database::table("files")->where("id", $thisFile->id)->delete();
            }
        } else {
            if ($actualFile == "original") {
                File::delete($fileId, "files");
            } else {
                File::delete($fileId, "files");
                File::delete($fileId, "copies");
            }
        }

    	return true;
    }
    
    /**
     * Record file history
     * 
     * @param   string $document_key
     * @param   string $activity
     * @param   string $type
     * @return  true
     */
    public static function keephistory($document_key, $activity, $type = "default") {
        $document = Database::table("files")->where("document_key", $document_key)->first();

        Database::table("history")
            ->insert(array("company" => $document->company, "file" => $document_key, "activity" => $activity, "type" => $type));

        return true;
    }
    
    /**
     * Save notifications
     * 
     * @param   int $user
     * @param   string $notification
     * @param   string $type
     * @return  true
     */
    public static function notification($user, $notification, $type = "warning") {
        Database::table("notifications")
            ->insert(array("user" => $user, "message" => $notification, "type" => $type));

        return true;
    }
    
    /**
     * Convert file to PDF
     * 
     * @param   string $document_key
     * @return  array
     */
    public static function convert($document_key) {
        $user = Auth::user();

        $document = Database::table("files")->where("document_key", $document_key)->first();
        $outputName = Str::random(32).".pdf";

        if (env('USE_CLOUD_CONVERT') == "Enabled" && !empty(env('CLOUDCONVERT_APP_KEY'))) {
            $api = new Api(env('CLOUDCONVERT_APP_KEY'));

            try {
                $api->convert([
                    'inputformat' => $document->extension,
                    'outputformat' => 'pdf',
                    'input' => 'upload',
                    'file' => fopen(config("app.storage").'/files/'.$document->filename, 'r'),
                ])
                ->wait()
                ->download(config("app.storage").'/files/'.$outputName);
            } catch (\CloudConvert\Exceptions\ApiBadRequestException $e) {
                return responder("error", "Failed!", $e->getMessage());
            } catch (\CloudConvert\Exceptions\ApiConversionFailedException $e) {
                return responder("error", "Failed!", $e->getMessage());
            }  catch (\CloudConvert\Exceptions\ApiTemporaryUnavailableException $e) {
                return responder("error", "Failed!", $e->getMessage());
            } catch (\Exception $e) {
                return responder("error", "Failed!", $e->getMessage());
            }
        } else if(env('USE_CLOUD_CONVERT') == "Disabled") {
            return responder("error", "Failed!", "Cloud Convert is not enabled, please enable on system settings page.");
        } else {
            return responder("error", "Failed!", "Your Cloud Convert API Key is empty.");
        }

        self::deletefile($document->filename, true);
        self::keepcopy($outputName);
        $data = array(
            "filename" => $outputName,
            "size" => round(filesize(config("app.storage")."/files/".$outputName) / 1000),
            "extension" => "pdf"
        );
        Database::table("files")->where("document_key", $document_key)->update($data);
        $actionTakenBy = escape($user->fname.' '.$user->lname);

        /*
         * Check, whether IP address register is allowed in .env
         * If yes, then capture the user's IP address
         */
        if (env('REGISTER_IP_ADDRESS_IN_HISTORY') == 'Enabled') {
            $actionTakenBy .= ' ['.getUserIpAddr().']';
        }
        $activity = 'File converted to PDF by <span class="text-primary">'.$actionTakenBy.'</span>.';
        self::keephistory($document_key, $activity);

        return responder("success", "Complete!", "Conversion successfully completed.");
    }
    
    /**
     * Check file orientation
     * 
     * @param   float $width
     * @param   float $height
     * @return  string
     */
    public static function orientation($width, $height) {
        if ($width > $height) {
            return "L";
        } else {
            return "P";
        }
    }
    
    /**
     * Protect file
     * 
     * @param   string $document_key
     * @return  true
     */
    public static function protect($permission, $userpassword, $ownerpassword, $document_key) {
        $user = Auth::user();
        $document = Database::table("files")->where("document_key", $document_key)->first();

        $pdf = new PDF();
        $inputPath = config("app.storage")."/files/".$document->filename;
        $outputName = Str::random(32).".pdf";
        $outputPath = config("app.storage")."/files/". $outputName;

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (env("PKI_STATUS") == "Enabled") {
            $certificate = 'file://'.realpath(config("app.storage").'/credentials/tcpdf.crt');
            $reason = $document->sign_reason.' • Digital Signature | '.$user->fname.' '.$user->lname.', '.self::ipaddress().','.date("F j, Y H:i");

            $info = array( 'Name' => $userName,  'Location' => env("APP_URL"), 'Reason' => $reason, 'ContactInfo' => env("APP_URL") );
            $pdf->setSignature($certificate, $certificate, 'information', '', 1, $info, true);
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $pdf->SetProtection($permission, $userpassword, $ownerpassword, 0, null);
        $pdf->numPages = $pdf->setSourceFile($inputPath);
        foreach(range(1, $pdf->numPages, 1) as $page) {
            try {
                $pdf->_tplIdx = $pdf->importPage($page);
            }
            catch(\Exception $e) {
                return false;
            }

            $size = $pdf->getTemplateSize($pdf->_tplIdx);
            $pdf->AddPage(self::orientation($size['w'], $size['h']), array($size['w'], $size['h']), true);
            $pdf->useTemplate($pdf->_tplIdx);
        }

        $pdf->Output($outputPath, 'F');
        Database::table("files")
            ->where("document_key", $document_key)
            ->update(array("filename" => $outputName));
        $actionTakenBy = escape($user->fname.' '.$user->lname);

        /*
         * Check, whether IP address register is allowed in .env
         * If yes, then capture the user's IP address
         */
        if (env('REGISTER_IP_ADDRESS_IN_HISTORY') == 'Enabled') {
            $actionTakenBy .= ' ['.getUserIpAddr().']';
        }
        $activity = '<span class="text-primary">'.$actionTakenBy.'</span> has activated document protection.';

        self::keephistory($document_key, $activity, "danger");
        self::deletefile($document->filename, true);
        self::keepcopy($outputName);

        return true;
    }
    
    /**
     * Sign & Edit document
     * 
     * @param   string $document_key
     * @return  array
     */
    public static function sign($document_key, $actions, $docWidth, $signing_key, $public = false) {
        if (!empty($signing_key) && !$public) {
            $request = Database::table("requests")->where("signing_key", $signing_key)->first();
            $sender = Database::table("users")->where("id", $request->sender)->first();

            $userName = $request->email;
            $user = Auth::user();
            $userName = $user->fname.' '.$user->lname;
            $signature = config("app.storage")."signatures/".$user->signature;
        } else if ($public) {
            $userName = "Guest";
            $signature = null;
        } else {
            $user = Auth::user();

            $userName = $user->fname.' '.$user->lname;
            $signature = config("app.storage")."signatures/".$user->signature;
        }
        $document = Database::table("files")->where("document_key", $document_key)->first();

        $pdf = new PDF(null, 'px');
        $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);
        $inputPath = config("app.storage")."files/".$document->filename;
        $outputName = Str::random(32).".pdf";
        $outputPath = config("app.storage")."/files/". $outputName;
        $pdf->numPages = $pdf->setSourceFile($inputPath);
        $actions = json_decode(base64_decode($actions), true);
        $templateFields = array($docWidth);
        $signed = $updatedFields = $editted = false;

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (env("PKI_STATUS") == "Enabled") {
            $certificate = 'file://'.realpath(config("app.storage").'/credentials/tcpdf.crt');
            $reason = $document->sign_reason.' • Digital Signature | '.$userName.', '.self::ipaddress().','.date("F j, Y H:i");

            $info = array( 'Name' => $userName,  'Location' => env("APP_URL"), 'Reason' => $reason, 'ContactInfo' => env("APP_URL") );
            $pdf->setSignature($certificate, $certificate, 'information', '', 1, $info, true);
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        echo "<pre>";
//        print_r($actions);
//        die;
        foreach(range(1, $pdf->numPages, 1) as $page) {
            $rotate = false;
            $degree = 0;

            try {
               $pdf->_tplIdx = $pdf->importPage($page);
            }
            catch(\Exception $e) {
               return false;
            }

            foreach($actions as $action) {
                if(((int) $action['page']) === $page && $action['type'] == "rotate") {
                    $rotate = $editted = true;
                    $degree = $action['degree'];
                    break;
                }
            }

            $size = $pdf->getTemplateSize($pdf->_tplIdx);
            $scale = round($size['w'] / $docWidth, 3);
            $pdf->AddPage(self::orientation($size['w'], $size['h']), array($size['w'], $size['h'], 'Rotate'=>$degree), true);
            $pdf->useTemplate($pdf->_tplIdx);

            foreach($actions as $action) {
                if(((int) $action['page']) === $page) {
                    if ($action['group'] == "input") {
//                        echo "Action => Input Group";
//                        echo "<br>";

                        $updatedFields = true;

                        $templateFields[] = $action;
                        continue;
                    } elseif ($action['type'] == "image") {
//                        echo "Action => Image";
//                        echo "\n";

                        $editted = true;
//                        echo $action['image'];
//                        echo "\n";
                        $image_64 = 'base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAACWCAYAAADwkd5lAAAAAXNSR0IArs4c6QAAIGlJREFUeAHtnQuQHEd5x7tndu8h3UnySYok8EuxsTEPV6EHkizjksviTRzesXEqFKCAnQJCHAoBNiUplkkZQiBgsKEcKhgHbKiYig0JGAsJsGTJlmwiHBzLNsIPsIWkk3R30t3t7kzn/83u7M3MzuzO7d5Z+/hPabQz/e5fz/U3/XV/PVpFDqPUy+G0DufrcL4V5z6coUMrhX/hA/HwL3wwnAeFXPi8yB8HnwM+B634HPwB/fiLwz37xF3Gv8TTncX1F3FehdMquZ9e+uUPCZAACZBA5xE4lKrKECBn4jyM08F5E861OLtSRWYgEiABEiCBtiMAGVAeZEjlcN+H88ZY2QDHV+Nc1XYUWCESIAESIIGGCUA+fBenwXljw4kxARIgARIggc4hAMFxIc48TtFUyXw5DxIgARIgARJIRwCC4xs4ZRTyRQv/vR+nzHeEdF3pkmIoEiABEiCBDiNwK+r7K5wnZFJkGKdIk74Og8DqkgAJkAAJNEKgJDzww4MESIAESIAE0hPw7T3Sx2BIEiABEiABEgABChA+BiRAAiRAAnURkInz9XXFZCQSIAESIIGOJIA5j2ul4hV7WnUkDVaaBEiABEggNQGZO5fAVGGlRsaAJEACJEACQQIUIEEavCYBEiABEkhNgAIkNSoGJAESIAESCBLIQJF1gzhgMoST6UEyvCYBEiABEkgisNnzkMkQf0IkKSTdSYAESIAESCBKgCqsKBHekwAJkAAJpCJAAZIKEwORAAmQAAlECVCARInwngRIgARIIBUBWqKnwsRAJEACJEACPgHMm9MS3YfBXxIgARIggfQE/IVXVGGlZ8aQJEACJEACAQIUIAEYvCQBEiABEkhPgAIkPSuGJAESIAESCBCgJXoABi9JgARIgARSEaAleipMDEQCJEACJBBLgCqsWCx0JAESIAESqEWAAqQWIfqTAAmQAAnEEqAAicVCRxIgARIggVoEaIleixD9SYAESIAEQgRoiR7CwRsSIAESIIG0BGiJnpYUw5EACZAACcQS4BxILBY6kgAJkAAJ1CJAAVKLEP1JgARIgARiCdASPRYLHUmABEiABKoQoCV6FTj0IgESIAESqEGAKqwagOhNAiRAAiQQT4ACJJ4LXUmABEiABGoQoACpAYjeJEACJEAC8QRoiR7Pha4kQAIkQAIJBGiJngCGziRAAiRAAtUJ0BK9Oh/6kgAJkAAJ1CDAOZAagOhNAiRAAiQQT4ACJJ4LXUmABEiABGoQoCV6DUD0JgESIAESqCBAS/QKJHQgARIgARJITYAqrNSoGJAESIAESCBIQOxAeJAACZxEAmbNmsxBd3SxVXBWKtes1Er3akvvtAr6p3MeeGD/SSwasyaBqgS0v55XK4V/PDqBwP41a3pmjo+faqmxM5Rln6cc97RQvbV9VCn3UVd1PXK8u/vZxdu2jYX8O/Am0Mm/TLvmLKPMAnT0B3A+anrsX8/dtuvZerAcW7VqIG/yV2plPoL4C0NpaP0j13I2zt/+8O6QO29IoEkIiAD5hJQF0uNzTVImFmOaCBxes+JUPVq4FI39AbT7klTZaLVdGfPljO6+d/b99w+mitNGgUTY9o+PnA8G78XfyNtRtXAn79VV35tT6vqFPX336W3bCmmrL+2hxgobEH5dlTi3qJ7MpnoFVJV06UUCdRNA/3GtROaoo26ErRNR3nILZnyt0vqjyqjVdZW8A9+GD6xYsSCj81fizwRnnOAIkISgNcbaOG/ng/cGXBMvi22SE+Hx0cRARY99WuuNA/fv/m6NcPQmgReMAAQI/inFSfQXDPnJyejg6tX9BZP7NDrBO+oWHlJ0Y95sufbGg6tftezk1OSFzXVw1dJX2jr/NXDbiJxjRh2R8niC2V0rI5aIT8WtqMPyVv498Hh3xPMA7n+A17rtAfdzjHHPCdzzkgSahgAFSNM0xdQXRISH7Y7K2/MVU5K6CBHH+pCnepmSBJszkcEVS853jdmI+Q1RWaU+MFJ4RX/u+CtrRTicO/4q7ao3IFxUMN2tkS8E/Z5wGnpARixhN96RwMknwFVYJ78Npq0Etjv2Z8Zo0a9HOyo/z3242GFsvQed1jOeo1anYZL4siqjlYv0uPMahG1LlYoIRzNW+EhpvsPnFPyVUYIcC4o/wf/NHO2oOUGX6LU3Qhkbfgfc3xzyk1GHq79nW13PYsQY8uINCTQrAVqiN2vLNFiuUkd4MZKpUH8YpR+GaubLWZ25K25iHCqWmwdzJ5Yr414jqqtIUaBSMUsxuvnh/O3bh8VPVDL+MlTLMS92bf17N2PvnG/17p/MpHIknxf81uvcx4feCTZviWR+QBl9q22sm2RZ7eEVy16rLLMhKmSxIqXX0ao3Ejd0KyMUGamAa8hdRh0ZKxsZeZSDDM3q7h4q3/GCBE4+gc1SBFFhySosbyWWOPBoDwK6UDgTk+aLY2qzDzr2L83tmXlbnPCQ8NLpz93xwP1YWvod3MooJXSg65vhFgozxFHUZEfGh/7OzhV+gZHLrUarf5RfuT+cH7lShEsochPf9OVHzlDGWoEiBkdsslT3n91M93W+TYabUU9DoBRHbIH6gMsSS7mvCjiFLj0B5brviBHKmChXu6Q9clZ+PlrgT0IRlSm0kiAOl5137UgAI/TPyMk5kHZsXamTo05HRxW27/Dqqh8yltqVpkMyrj4IYXMwisgyema3MZ4AUc74xa6xLkeYYKcrURbCvuTiI6NDL4vGb9Z7W5v5qC868PLhCQ/H7r7JH22JTzZv5TDamLSe6ZQTJxaB3aJy6qUL2JQ8AgPCR+S2ogzeMmprRzQO70mgGQhQgDRDK0xDGaBmegmSrVBfQeE0YmXt441kabQ5qm37mLxRY5RyAc7Yt26oahYYZcXMFTSS+zTGzZshrHg6FsjhtqjwEL981u3CSKsrEM6/lCW3j/s30V9Hu2cby5wVdccIZ7+T6d0vPN2CegsE/yUTYfSYzprRiXtekUDzEKAAaZ62eEFKgrfdAT2aT7eiR7sXRPX8pULmc5aVr1VgCDG8wbv+pHOt4Cfd/5TeWb/JK+srUEXdhE59PQz4vhQcedQsIEZr3qgtJqARVZ42F8XwBCP1pOQjKjTke0YoujH/aztdvwm58YYEmoSA6KfXN0lZWIwpJICO6I/oCKXzDo0A4H4BJoBfj7mL/bU6R1dZD6NzwzkxwpAJeKT7Cz8u7CX2Yj5Y5kkqRjvIC2/WPfunsFrTmlRJrbcNmciZeNgFt9vFCAT1SwwT9RhUo6Lii6r5MN2hH1G2+XXJKv3jCHNZOW5xZdYPZ+/svB0Aygx40ZQE0Ad4lugZ/AlwC5OmbKLGCoXlu09AUDyBN96QAEGqC+F3teWMzoCl9c0Ldu1KHCG4xt5l68JOxPFVVAcwZL0dap2tfukcq+du2x3vx2jjarj5QgRp6psLxr5pQWmllh++HX5FLae1WQC2kUMfxZJo7CNWeZicuwjqrUUxq68OqoLu13nneqjF/ioUs/rKrFBQ3pDAC0zgOsmvZVbIvMBwWj+7XvtR7LP0KCqyOqYyeBPWGzNW4bWHVy5J3OdKhAtGKuuNOvFN27HnG+M8dax39m8XBzZXLI1Evg5Dt++PK+Wpxlx9fGhB18Bgmon6mLI1vZO21DyMuoKT7cUyGzWMOY7Y5bbGNYssZRZVyBwsgobbGgxmwqMTbB1jtL5t9g6OPpr+gejgAlKAtGnjy+Z7UC9tRUd3EarojwzCtfW239CrYbi2b3DVklu06fr+nJ6eZ4Mdf0lAVN0NViZ/c/ljPRlt5RzTc2Th9r2efUg4s/g7WQZs67FTLFdlldU7OOe++47Ehyy6Sl59+WMDWWP12rY71GfPPhIsr4Tyw9jaykh5fHVbtXQn41fcWkRXMMX80hG3kIkdgcjyXqj/lsTkExYcEqAoPDbN2/HggzHhE53EWt3o3Cl57Y6OZGcPBgV9NJLPKA1HiVsrPQkzmfwlPI/WJ5Beidv6de24GnidszN+FTo2US9FVVlxPJ7H2/Cd6MC+Ndzdt7daB+RH9rb90GoD5gPeXnJ7Hg/VpoGde272w0R/pfOanR9ablxrfcgmAjr/ahsSRvNCPg85Sm+av3P3XZKHt/mhnb9Uu/pK1KPUWet7dcZsGrhvz33RctRzL50kBO4GxK3YBBGc74Sa6naMHCaW+MKyH8aVImxWT5SpSs6TFB5Fg1F3rdbuusgE/fNYcQc1YrZCTZmKo5W/HLYuV6GkJUGpb3dtha3ldz8WLH09+Qfj87o1CeBZxj+86+D/G7wLTqa3ZkvWKLUnRNzx90TmKGrEgjc6Mm25NxzLznowSZCUJn43IPS6SIJQi3VtijNUPLh62bmqoK60NLZLiZtUnkhon7H05rldfd+VEUZyXmbj3J0PbTp0wfLl2EdqQ0ggTaRV95boJSF8oWuZK7B/lSyvrRwxTOTTyNUBzIHcjjrD5iTcScclarCq6/DY8UsxsrkGf8MlYRkXMtzxN8JR+orhnv6N8jzUm39cCenWegTwLHhzILKMl5bordd+qUss6ptTuvv+Naf0hyAUtqSOiC1MjKO/1z82tOnoq1+9ODbeqHMedPfnxfrFOEonD1XVFyA8PgbvWh3xOeiw33QoP3KWJOWOOUugAloaSdazu6ghPBBF97l5Z2Ykbs1bGSlZ7ii+nWK+ibJcgQi1ylwzzdgAENb4sNfb5nb3f3xqhYfkZi7DCOivRRDKXSMcMeLrHRgfn5FeeFTmLy48Wp8AngVaord+M6argbzBL9q5e1tGZd8tHQo6/eB24dUSWQjV1Ccc2/1K3DbuWIl0NgayZ8ckULF3U+1OPiaVkpN0WJiAXh5cTux5wWgPQqVXuc4nE0YeyYmm8JmlRudBjfNyBJ0WwSFLoqU9hrr73ultHZPyY1SDoyMXQ2V1dfWRR7CC+sVKjb9oqjjWm3+wRLxuDwI0JGyPdkxVC1EpQd1zx1B3/1ro6VegA7oZEbHktsaB0Yh29KdEd+6HlLdzV6kzcB+dWxHDuD+I0PLDytyEct33JXTykn/0GxheVNkuRLYN+ePo6FyUda6fXuT3bYH5l4hXY7f2CZPVRnc1lkpybHB6zLWtX4lKKDlU2EdGg4j3l5H5jmKg4ovBD3ATblNszyJbpDTKEW0wWnDH8FJRX/7hmvCuHQhwFVY7tGKkDtK5+yuV4lY2lTqsB/BG+tDRsbEbjMm/ydV6XcUbfiBd6aRhPPc40t4n8WeMjs7GSGZWIEjxUqsnYI39hO+OPDLYVPFd2Ob8bb6b/1t8A9eb4fcYjOlgmIQuKnB4W6ZY9jEYvc+HADsl6g+BFFVpBWJPXEIFlbg6aiJU5dWRGTOemzU28iv4vB5nVFBWRoCL1AnzElgMoAfB5xmj3f2Wa51bi29sYjGOru2swsq6lTFeX3Ctnk0y0rActVFGNhNhTI/J616sScN28/VzhOB4r9L2GvBcOJF2+apm/uWQvGgbAiJA1rdNbVgRVZwkHd6AjZbXOeCBFZ0PHVq5/HrsvntXcFQgqEr3v8Pl17C66Pa8wffSlflokiDBCiP/g0kPJndG+hlvt1rJAIfMYaBDE1uUaKdT3BW4e+ZdohLBco7TIvJDonuqsMHR/FIYRcb5R9OUOBUHOr58T4qtV6IRRVBCYN40a/TYT7S2z8AqpLOqfCtl80BP/6YoY0kT279bUPediKlfNMuq9zKPYZwxmTAPLyEurty6Q+a7oCqcBZb9obywc7C0iVXABpsNcES+4B0jPFLmX7Vy9GwpAnjV8yzRLfxxfU7Olio9CxtLwJsoHSvIBPU6PwAaeollzJ8fO3EiZmdeP5RSot7C97z/TVneZPuPJnyCV/LBJDNHXLzOSDr9iiO8WaPtyijBVKwSQrm2OK79E+lwE+ZSyqqwBP9wzrJAwKh/iJ/fMYNxK8LCCcTfiRCZu+vh3wzs3P3fmA/ZBbF7NCakbMf+aJzwiAk74aRVP1RklaO4iRCRq/EXIX/MZ0QOY57MmsyTot7ChPmHo6rC4gjMPto4x0i+/m3K/P3g/G0LArIK6zrOgbRFWxYrYRdGF6OzWBytEpaHdskOslH3uHvPeM1V/xLbEYvhoWywKEfyRovH3HzG29HWMywzagVCh9+Y8Y0RvLncJ5bueMOPn0sRVZhsx4IDdZLvY1RTIWEJrPlPY6tDCHy2xAkcVXfIDYSreQlBjEUD3i7H4bCYzHe19XjYMc3dhEBOEzpJaEMYZx0z/kbHdr4NDqHtUESlhvPHwjotR4w0Zfua1Efa/FMnyIAtQ0BUWDzahEDyHk2Tq6Cn7nDkg0noGiZ9mGHf8ls+jmQ58nGkcDroyB7Bsljv+xeyLLSA5aHRbGQnX6y8Ck8GRwOV7/VWCMl7LMe9DJItLGiq7JBbjp7iQuZyBseG5dsmUWEIWaoO20YfSkqmXp6S5/NjIxdC8l8DgT4OQbAd6sWKbKBBgLEojP4qvRDN/MzYPVsrIsU6FDlijuV0REx9TF3+qbNkwCYhQAHSJA0xvcUwf+xyuyo+DJWUp6dWierRi4G9t/mSNfZANH7pbRdLU4tH0q61mJB/Vr79LaGShIxlrBGcw6WOu9pzegD7T+1wTP6IpbIVZRKVU9IGh6VipvqRFUy2VnPj+lW89R/MZ7KJAiQxA29EZy5CHbfGqb+OFIZXdil9DQTwWhEOUJOhflKCGElRmck+1PsrtnHu1s7opYOrlq0y8rXE5Kgex3nZ/icPqREFwR/3AjFYyiaGc0UBwvmvXLoCnP5roHvWvXF1rYhNh5YgwG+it0QzNVZIvPGf6rjjpyIVvwNITFA67MO5kZcmqGqewWqpp/G1w9hDJoqxjcio75lqROSoc9GnnRvtmNHZHNVYgTU0Pi5zBInzBKjbdkzS/xyfIofcK2Sj6aDDTNzg0C9nmt+syi/AsmWMbipyQPSJUVdcWm5B5h8KR+JiIvzC0lbvnkCV+J7QHB1dZhzYt4jwKB9muHxZ5QJM7oQwvz6re/a77ol1uL4a7YkJ8IQSIK0yR8xJIf8nDzvHvw9ntEtxK395OUD7bkEaEELBD15VFiQ2f9RTu9ZszMXJVij7K2PRpcUIbJby0hK9xVqtWnHl402i+omGKdpJWH+faFFeiiD2GoO5ofdZrmcpXqGqQcex32Qyv4v/brckMrm3fRnJQE11id9JRcpdYYwY8S/d6seP9c7a560KU7JEdXoOR1tnWkqfWZG62F6Y6p+cPdHbK3NCQxVxiw5vUeOF9z9/wfkyz+OtojsyOvxBCJyvgvebQ3FkabCccQcWEWhLX45R2OK5PbP+Qtk9j7sOhAe27kdwCI9aR5GjhJIRAhZU3Kl77EuxDHktrOQvyOrsWtfSt0DSxI9kU+Q/mbm4WqWl/8klgFcRzxK9mmrg5JaQuU+agG11P2tMrvwmG0xAJlcd7bzu0AVLt+BVXeYMyioXGOxhe3KzRKvCWuXqcxK0HPtgfPjLuar3eag4vO1Fgul715G3/WTdv34ZlgyvgTX1KxAP1vFxhylIR4a34aHBsVxs51tUmakdi7FSCkaOk/7IU1yucW7eiGBseDm4LKnwLy2RrXAPOEj5sLz2PyAQXlEhFKRzN2pT1mQ3HV6JBWtjBciIQOTSpbzVo92+iXaE4WTMgQAutuJVOt8zmHOX43v070Z7YU6otvAIcgymLDs64778POEFI5s4kkqRv28YGsyD161NgAKktdsvVHpZqnpw5bIt6GBek/BWDzWC7OmEjQEjUiK2Uwqlrn6cVdkfSqc+eOHS+eic5qPjCx3RDkKsyF3t5io7RLMW5fP0+qEE/Bt5q3eLb/WSH75Zgnn2uBK6+22jfifRktVlsqw409A34Kur0dLNLw13zfx1/9jwI6hFeFTh17nKr3TwUBb8+0DvzK2wmcGlOR8VXh2OEmBqHKzGDvtWv5vgWC2crOSCTcsP6s3f1ea5Y70znquWB/1aiwCX8bZWe9Us7UhP3z3oO76NgBWqrJqRkwPIbraf920pxKoZXXZPNLg3b2Hb3hJe8Zs9Y8Yzrm1243KSZQmrwrCsVJbIymdzQwdUc/hkbu/+kGPlzYnMuHWi0jm9S7LKTomtyv/5XKqlKKMQLAP+jowkqoWL+hWFB3YlLhmCZqzsHgiPPdFwte4xerkVaV2LcPVy9LJoJP+Ma39dONQqK/1bh4AIkPWls3VKzZImEpA/0KGevpvQwf9tsfNJDFrbw9tbyVyGrdnXl9QZVeNgQDI62N1d7qxl9KC7MrI3091VI0Y8ZVlsphBYFmurp7EECauCQod03k/6S4aLK61ijfxCkab0RuvdrmWJgEx1DOx6aC9GSt9K3S6YV8grdbUvPCQTEVa2a39JBEKqTCUQLMVdW99oZczPa3GslWYj+c954IFawr5W9vRvEgL4W79WTlqiN0mDTGUxRIjIpomOsd8Ihfo70Wlg9cwkDtleXJs3ZFTXpZJO9A1b5jaQbqhDl04RpzcfEcxJBA86vM9OpsOLjmTwVcH/gXrsHqQbGMnorY6tf+bnJZbYmF940r8v/YqF+K5o+SNhat6OZPuewsjhqUjAAxgJbBHVVMS96q18/AoGj5ejV7+9SkCMEvTVBde+QnZRFkEcDCsdsZPp/jBGZlfCvWJEEQjrp/MBMRBNwzEQN/Gy3vwTE6RHKxK4DoW+Di9xPNqdwH5Ye2M/pz+V/ZyU5c51Xet0bPi3SOqNLwBi2S1WblnYK0m70kE/dbRr9gERQklcvEnl48fPNV3mYo39lQwEis7prQMzZz4W7ez8NKQMc3LHX+q67jJ0vKchv0sq9fh+aFWxr5Rne6LGl6O8S/Ghq+dUV/an0VGRhMmZ/IVQFWE2Wg1h88et83r69yaVqZxbiotI2oih7xvq7ftlNU7VkvXbxFj2MuyvdRbS6xWOsNvYm3O6HpP5hmrxxU/a4UBucCCres7CAoizpF0haOdUSycNx1r5+v715O/H5W9rE8DoA//w1LZ2NVj6ViMgnU6NL+nJqGHTwP17vtNqdWN5SaBTCPgChJPondLiTVDPFMIDrzRQjcmcBw8SIIGmJ0BL9KZvovYooOwUfGR86EqsnJIv6SUatmEC/feWY/++PWrNWpBA2xLYLDUTO5BPlKooq7F4kMCUE/BGHrmRt8Iqeh0STxQekrE3gY4tTKa8EEyQBEhgyghg7uMzkhgNCacMKROKIzChtjIfw8ijcnuUikj1f7ujIik6kAAJTCsBzoFMK14mLjvKYhO+v4HwKG8D4tlBaPVPFcuLZT+lzCSXHBMxCZDASSNAAXLS0Ld/xrJk1BTUu7BW+JJAbcUA8NuYLX8U7qcF3KG/Km7WGHLjDQmQQNMSEBUW5z6atnlau2COyr0BNZAzeNzh2s4d2rHw7fWwSgujlMPDqre8yWMwEq9JgASahwD+VmVbHJXBHzG/h9487dI2JTm8ZsWpZqxwMSo0Me8BC3dswnibXTDzsLHeS4JmSEVL9uLOum0DgRUhgfYlIJbo3vdA2reKrNnJIzDqnAcz1fOCBcCW8Y94W39ovRrLeUO7yWJ33j1Wj/1QMDyvSYAEmpsA50Cau31atnSYOD8bmx2cHajAPmy8trcvP3IGRhsXwX1BwA+fU1V7B/CtkYAbL0mABJqcAAVIkzdQKxZPlu5CR/oilL0sJKAqHcEkubZd91LZxSlcL/nAlb5nKvasCqfLOxIggekkQEv06aTLtMsEist49W3FLdjKznKB0YfZMS/bH91JNxSINyRAAk1FYLOUBpt3lndVxEsiDxKYGgKDK5dehWdrA1Irj0ISUpaPVW2K7qybEJbOJEACTUSAKqwmaox2Korl2j/GN0B+UrVOsirLsr5B4VGVEj1JoGkJUIA0bdO0dsHko0PyFTxYm/8oribyaVftup+WDx3F+dONBEig+QlQhdX8bdTSJTywYsWCjHIuhKb0fGhMe/F7AF/S2+nY3Xv9z9G2dAVZeBLoYAIiQLzdeGlQ2MFPAatOAiRAApMgALnhWaJz4nwS0BiUBEiABEjAW3kFGUJLdD4LJEACJEACdRLgJHqd4BiNBEiABDqdAAVIpz8BrD8JkAAJ1EmAluh1gmM0EiABEuhgAp4lupihGzk7GASrTgIkQAIkUAcBqrDqgMYoJEACJEACXIXFZ4AESIAESKBOAhyB1AmO0UiABEig0wnwm+id/gSw/iRAAiQwSQKYNKcl+iSZMTgJkAAJkAAI+AuvqMLi40ACJEACJFAXAQqQurAxEgmQAAmQAAUInwESIAESIIG6CNASvS5sjEQCJEACHU2Alugd3fysPAmQAAk0SIAqrAYBMjoJkAAJdCoBCpBObXnWmwRIgAQaJEAB0iBARicBEiCBTiVAS/RObXnWmwRIgATqJEBL9DrBMRoJkAAJdDoBWqJ3+hPA+pMACZBAgwQ4B9IgQEYnARIggU4lQAHSqS3PepMACZBAgwRoid4gQEYnARIggQ4kQEv0Dmx0VpkESIAEpowAVVhThpIJkQAJkEBnEaAA6az2Zm1JgARIYMoIUIBMGUomRAIkQAKdRYCW6J3V3qwtCZAACTRMgJboDSNkAiRAAiTQmQRoid6Z7c5akwAJkMCUEeAcyJShZEIkQAIk0FkEKEA6q71ZWxIgARKYMgK0RJ8ylEyIBEiABDqGAC3RO6apWVESIAESmAYCVGFNA1QmSQIkQAKdQIACpBNamXUkARIggWkgQAEyDVCZJAmQAAl0AgFaondCK7OOJEACJDCFBHxL9ClMkkmRAAmQAAm0OwEIjwGxRMc5TBVWu7c260cCJEACU0vgpaXknqYAmVqwTI0ESIAE2p3A+0sVvMcTIBiKzMb5VZyr2r3mrB8JkAAJkEBDBJ5D7DzOW7xUIDg+i1N0WgdxLmsoaUYmARIgARJoawKQE6eVK4ibLpxbcIoQcXB+suzJCxIgARIggY4kAFmwAOfrcM6rCgABRIjciFMEyAeDgUt+IlyC53gwjFwznMeHXIrPUvBZkWtyIRc+B63bT34q2t/LvdiBeIdWKoeLD+MP/Ub8/rboyv9JgARIgAQ6mMAI6v50Uv3/H9a7IQZGl7WSAAAAAElFTkSuQmCC';
                        $imageArray = explode( ',', $image_64 );
                        $imgdata = base64_decode($imageArray[1]);
//                        $imgdata = config("app.storage").'assets\images\signhere.png';
//                        echo 'IMg => ' . $imgdata;
//                        echo "\n";

                        $pdf->Image('@'.$imgdata, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', false);
                    } elseif ($action['type'] == "symbol" || $action['type'] == "shape" || $action['type'] == "stamp") {
//                        echo "Action => symbol, shape, stamp";
//                        echo "\n";

                        $editted = true;
                        $svg = str_replace("%22", '"', $action['image']);

                        $pdf->ImageSVG('@'.$svg, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', 0, false);
                    } else if ($action['type'] == "drawing") {
//                        echo "Action => Drawing";
//                        echo "\n";

                        $editted = true;
                        $imageArray = explode( ',', $action['drawing'] );

                        $imgdata = base64_decode($imageArray[1]);
                        $pdf->Image('@'.$imgdata, 0, 0, $size['w'], $size['h'], '', '', '', false);
                    } else if ($action['type'] == "signature") {
//                        echo "Action => Signature";
//                        echo "\n";

                        $signed = true;

                        if (!$public) {
//                            echo "Action => Signature (Not Public)";
//                            echo "\n";
                            $pdf->Image($signature, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', false);
                        } else {
//                            echo "Action => Signature (Else)";
//                            echo "\n";
                            $imageArray = explode( ',', $action['image'] );
                            $imgdata = base64_decode($imageArray[1]);

                            $pdf->Image('@'.$imgdata, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', false);
                        }
                    } elseif ($action['type'] == "text") {
//                        echo "<pre>";
//                        print_r($action);
//                        echo "</pre>";
//                        echo "Action => Text";
//                        echo "\n";

                        $editted = true;
//                            - 6
                        $pdf->SetFont($action['font'], $action['bold'].$action['italic'], $action['fontsize']);
                        $pdf->writeHTMLCell( self::scale($action['width'] + 90, $scale), self::scale($action['height'], $scale), self::scale($action['xPos'], $scale) - 3, self::scale($action['yPos'], $scale), $action['text'], 0, 0, false, true, '', true );
                    }
                }
            }
        }
//        die;
        $pdf->Output($outputPath, 'F');
//        echo "<pre>";
//        print_r($templateFields);
//        die;

        if (count($templateFields) > 1) {
            Database::table("files")
                ->where("document_key", $document_key)
                ->update(array("filename" => $outputName, "editted" => "Yes", "template_fields" => json_encode($templateFields, JSON_UNESCAPED_UNICODE)));
        } else {
            Database::table("files")
                ->where("document_key", $document_key)
                ->update(array("filename" => $outputName, "editted" => "Yes"));
        }

        if (!empty($signing_key)) {
            $request = Database::table("requests")->where("signing_key", $signing_key)->first();
            $sender = Database::table("users")->where("id", $request->sender)->first();

            Database::table("requests")
                ->where("signing_key", $signing_key)
                ->update(array("status" => "Signed", "update_time" => date("Y-m-d H-i-s")));
            $notification = '<span class="text-primary">'.escape($userName).'</span> accepted a signing invitation of this <a href="'.url("Document@open").$request->document.'">document</a>.';

            Signer::notification($sender->id, $notification, "accept");
            $documentLink = env("APP_URL")."/document/".$request->document;
            $send = Mail::send(
                $sender->email, "Signing invitation accepted by ".$userName,
                array(
                    "title" => "Signing invitation accepted.",
                    "subtitle" => "Click the link below to view document.",
                    "buttonText" => "View Document",
                    "buttonLink" => $documentLink,
                    "message" => $userName." has accepted and signed the signing invitation you had sent. Click the link above to view the document.<br><br>Cheers!<br>".env("APP_NAME")." Team"
                ),
                "withbutton"
            );

            if(!empty($request->chain_emails)){
                self::nextRecipient($request->id);
            }
        }

        $actionTakenBy = escape($userName);
        /*
         * Check, whether IP address register is allowed in .env
         * If yes, then capture the user's IP address
         */
        if (env('REGISTER_IP_ADDRESS_IN_HISTORY') == 'Enabled') {
            $actionTakenBy .= ' ['.getUserIpAddr().']';
        }

        if ($updatedFields) { 
            $activity = '<span class="text-primary">'.$actionTakenBy.'</span> updated template fields document.';
            self::keephistory($document_key, $activity, "default");
        }
        if ($editted) {
            $activity = '<span class="text-primary">'.$actionTakenBy.'</span> editted the document.';
            self::keephistory($document_key, $activity);
        }
        if ($signed) { 
            Database::table("files")->where("document_key", $document_key)->update(array("status" => "Signed"));
            $activity = '<span class="text-primary">'.$actionTakenBy.'</span> signed the document.';
            self::keephistory($document_key, $activity, "success");
        }

        self::deletefile($document->filename, "original");
        self::renamecopy($document->filename, $outputName);

        return true;
    }
    
    /**
     * Scale element dimension
     * 
     * @param   int $dimension
     * @return  int
     */
    public static function scale($dimension, $scale) {
        return round($dimension * $scale);
    }
    
    /**
     * Scale position on axis
     * 
     * @param   int $position
     * @return  int
     */
    public static function adjustPositions($position) {
        return round($position - 83);
    }
    
    /**
     * Get Ip Address
     * 
     * @param   int $position
     * @return  int
     */
    public static function ipaddress() {
        $ipaddress = '';

        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if(getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if(getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if(getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if(getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if(getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }
     
        return $ipaddress;
    }
    
    /**
     * Send the next request on queue
     * 
     * @param   int $requestId
     * @return  true
     */
    public static function nextRecipient($requestId) {
        $request = Database::table("requests")->where("id", $requestId)->first();
        $sender = Database::table("users")->where("id", $request->sender)->first();

        $emails = json_decode($request->chain_emails, true);
        $chain_positions = str_replace( array('["[',']","[',']"]'), array('[[', '],[', ']]'), $request->chain_positions );
        $positions = json_decode($chain_positions, true);
        $actionTakenBy = escape($sender->fname.' '.$sender->lname);

        /*
         * Check, whether IP address register is allowed in .env
         * If yes, then capture the user's IP address
         */
        if (env('REGISTER_IP_ADDRESS_IN_HISTORY') == 'Enabled') {
            $actionTakenBy .= ' ['.getUserIpAddr().']';
        }

		$activity = 'Signing request (Chained) sent to <span class="text-primary">'.escape($emails[0]).'</span> by <span class="text-primary">'.$actionTakenBy.'</span>.';
		self::keephistory($request->document, $activity, "default");
		$chainEmails = $chainPositions = '';
        
        if(count($emails) > 1){
            $chainEmails = $emails;
            unset($chainEmails[0]);

            $chainEmails = array_values ( $chainEmails );
            $chainEmails = json_encode($chainEmails);
            $chainPositions = $positions;
            unset($chainPositions[0]);

            $chainPositions = array_values ( $chainPositions );
            $chainPositions = json_encode($chainPositions);
        }
        
    	$signingKey = Str::random(32);
		if( env('GUEST_SIGNING') == "Enabled" AND env('FORCE_GUEST_SIGNING') == "Enabled") {
		    $signingLink = env("APP_URL")."/view/".$request->document."?signingKey=".$signingKey;
		} else {
		    $signingLink = env("APP_URL")."/document/".$request->document."?signingKey=".$signingKey;
		}

		$trackerLink = env("APP_URL")."/mailopen?signingKey=".$signingKey;
		$receiverData = Database::table("users")->where("email", $emails[0])->first();

		if (!empty($receiverData)) {
		    $receiver = $receiverData->id;
		} else {
		    $receiver = 0;
		}

		$requestData = array( "sender_note" => escape($request->sender_note),  "chain_emails" => $chainEmails, "chain_positions" => $chainPositions, "company" => $sender->company, "document" => $request->document, "signing_key" => $signingKey, "positions" => json_encode($positions[0]), "email" => $emails[0], "sender" => $sender->id, "receiver" => $receiver );
		Database::table("requests")->insert($requestData);
		$send = Mail::send(
            $emails[0], $sender->fname." ".$sender->lname." has invited you to sign a document",
            array(
                "title" => "Document Signing invite",
                "subtitle" => "Click the link below to respond to the invite.",
                "buttonText" => "Sign Now",
                "buttonLink" => $signingLink,
                "message" => "You have been invited to sign a document by ".$sender->fname." ".$sender->lname.". Click the link above to respond to the invite.<br><strong>Message:</strong> ".$request->sender_note."<br><br>Cheers!<br>".env("APP_NAME")." Team
                <img src='".$trackerLink."' width='0' height='0'>"
            ),
            "withbutton"
        );

        return true;
    }

}
